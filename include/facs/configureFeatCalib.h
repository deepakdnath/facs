/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     configureFeatCalib.h
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents:
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#ifndef NL_CONFIGURE_FEAT_CALIB_TYPES_H
#define NL_CONFIGURE_FEAT_CALIB_TYPES_H


#include <string>
#include <vector>
#include "timeutils.h"
#include <opencv2/opencv.hpp>

using namespace std;

namespace NorthLight {


////////////////////////////////////////////////////
/////// Enums
////////////////////////////////////////////////////

/** features to be used for calibration
 *///compiler checks for it, better than define, compiler sees if any enums are missing switch
enum FeatureDetectorType {
    POINT_FEATURE_TYPE_EMPTY,       // Invalid feature type
    POINT_FEATURE_TYPE_CORNER,      // Checkboard features
    POINT_FEATURE_TYPE_SIFT,        // Sift implementation in Opencv
    POINT_FEATURE_TYPE_SURF,        // Surf featues available in Opencv    
    POINT_FEATURE_TYPE_STAR,
    POINT_FEATURE_TYPE_ORB,
    POINT_FEATURE_TYPE_SIFT_VLFEAT, //Sift implemented as per vl_feat library
    POINT_FEATURE_TYPE_MSER,        // Maximal Stable Extremal Regions
    POINT_FEATURE_TYPE_FAST,        //
    POINT_FEATURE_TYPE_GFTT,
    POINT_FEATURE_TYPE_HARRIS,
    POINT_FEATURE_TYPE_DENSE,
    POINT_FEATURE_TYPE_SIMPLEBLOB,
    POINT_FEATURE_TYPE_GRIDFAST,
    POINT_FEATURE_TYPE_PYRAMIDSTAR,
    POINT_FEATURE_TYPE_SIFT_HESS,   //Sift implemented by Hess
    POINT_FEATURE_TYPE_SIFT_UTKA   //Sift implemented by Utkarasha};
};
    
enum FeatureMatcherType{
    MATCHER_TYPE_EMPTY,
    MATCHER_TYPE_BRUTEFORCE,
    MATCHER_TYPE_BRUTEFORCE_L1,
    MATCHER_TYPE_BRUTEFORCE_HAMMING,
    MATCHER_TYPE_BRUTEFORCE_HAMMINGLUT,
    MATCHER_TYPE_FLANN
};


/* Fundamental matrix estiamtion method, extensible
 * */
enum FunMatMethod {
    FUN_METHOD_EMPTY,
    FUN_METHOD_7POINT,
    FUN_METHOD_8POINT,
    FUN_METHOD_RANSAC,
    FUN_METHOD_LMEDS,
    FUN_METHOD_5POINT
};

/** Recovering R adn T methods
 */
enum PoseMethod {
    POSE_METHOD_EMPTY,
    POSE_MEHTHOD_BOUGUET,
    POSE_METHOD_8POINT,
    POSE_METHOD_8POINT_BA // WITH BUNDLE ADJUSTMENT
};

/** Auto Calibration methods
 */
enum IntrinsicMethod {
    INT_METHOD_EMPTY,
    INT_METHOD_BOUGUET,
    INT_METHOD_TSAI,
    INT_METHOD_HEIKKILA,
    INT_METHOD_ZHANG
};

/* Bundle optimizer types
 * */
enum BundleOptimizerType{
    BA_TYPE_MOTION, //known 3D points, optimize for K,R,T
    BA_TYPE_MOTION_STRUCTURE_POSE, //unknown 3D, known K, optimize for R,T
    BA_TYPE_MOTION_STRUCTURE_AUTO, //unknown 3D, unknown intrinsics, optimise K,R,T

};

/* Noise types
 * */
enum NoiseType{
    N_TYPE_THERMAL, //THERMAL NOISE
    N_TYPE_SALTPEPPER, // SALT PEPPER NOISE
    
};


/* Noise types
 * */
enum DistortionType{
    D_TYPE_RADIAL, //RADIAL DISTORTION
    D_TYPE_TANGENTIAL, // TANGENTIAL
    
};


////////////////////////////////////////////////////
/////// Structures
////////////////////////////////////////////////////

struct featuresSpeed{
    struct timespec detectionTime;
    struct timespec descriptionTime;
    struct timespec matchingTime;

    featuresSpeed(){
        detectionTime.tv_sec=0;
        detectionTime.tv_nsec=0;
        descriptionTime.tv_sec=0;
        descriptionTime.tv_nsec=0;
        matchingTime.tv_sec=0;
        matchingTime.tv_nsec=0;
    }
};


struct ImageFeatures{
    bool isValid;
    FeatureDetectorType fType;
    FeatureMatcherType mType;
    std::vector<cv::Point3f> objpoints;
    std::vector<std::vector<cv::KeyPoint> > multikeypoints;
    std::vector<cv::Vec2i> matchIdxs;
    struct featuresSpeed fspeed;

    ImageFeatures():
        isValid(false),
        fType(POINT_FEATURE_TYPE_EMPTY),
        mType(MATCHER_TYPE_EMPTY){
            objpoints.clear();
            multikeypoints.clear();
            matchIdxs.clear();
        }

    ~ImageFeatures(){}
};

struct calibrationSpeed{
    struct timespec calibrationTime;
    struct timespec optimizationTime;
    calibrationSpeed(){
        calibrationTime.tv_sec=0;
        calibrationTime.tv_nsec=0;
        optimizationTime.tv_sec=0;
        optimizationTime.tv_nsec=0;
    }
};

struct CameraParams{
    cv::Mat K, D, R, T, F, E;// ,Tensor;
    CameraParams():
    K(cv::Mat::eye(cv::Size(3,3),CV_64F)),
    D(cv::Mat::zeros(cv::Size(1,4),CV_64F)),
    R(cv::Mat::eye(cv::Size(3,3),CV_64F)),
    T(cv::Mat::zeros(cv::Size(1,3),CV_64F)),
    F(cv::Mat::zeros(cv::Size(3,3),CV_64F)),
    E(cv::Mat::zeros(cv::Size(3,3),CV_64F))//,
//    Tensor(cv::Mat::zeros(3,cv::Size(3,3),CV_64F))
    {}
    ~CameraParams(){}
};


struct CalibFeatures{
    FunMatMethod fMeth;
    PoseMethod pMeth;
    IntrinsicMethod iMeth;
    std::vector<CameraParams> multicamparams;
    struct calibrationSpeed cspeed;

    CalibFeatures():
        fMeth(FUN_METHOD_EMPTY),
        pMeth(POSE_METHOD_EMPTY),
        iMeth(INT_METHOD_EMPTY){
            multicamparams.clear();
        }

    ~CalibFeatures(){}
};

}; //namespace

//NL_CONFIGURE_FEAT_CALIB_TYPES_H
#endif
