/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     utilsFeatCalib.h
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#ifndef NL_UTILS_FEAT_CALIB_H
#define NL_UTILS_FEAT_CALIB_H

#include "configureFeatCalib.h"
#include <sstream>

namespace NorthLight {
    
    class MultiCamPoints{
        
    public:
        MultiCamPoints(const uint8_t a_numCameras);
        ~MultiCamPoints();
        
        void add2DPoints(const std::vector<cv::Point2f>& a_imgpts, const uint8_t& a_numCameras);
        
        void add3DPoints(const std::vector<cv::Point3f>& a_objpts);
        
        bool checkConsistency();
        
        
    private:
        
        uint8_t numCameras;
        std::vector<cv::Point3f> ncam_objpts;
        std::vector<std::vector<cv::Point2f> > ncam_imgpts;
        
        
        bool get2Dpoints(const uint8_t& a_numCameras, std::vector<cv::Point2f>& imgpts);
        
        bool get3Dpoints(std::vector<cv::Point3f>& objpts);
        
        uint8_t getNumCameras();
        
    protected:
        
        friend class Calibrator;
        
    };//class
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Conversions
    ////////////////////////////////////////////////////
    
    cv::Mat convertVecPointToMat(const std::vector<cv::Point3f>& pts3d);//, cv::Mat& outMat);
    cv::Mat convertVecPointToMat(const std::vector<cv::Point2f>& pts2d);//, cv::Mat& outMat);
    cv::Mat convertToHomogeneous(const std::vector<cv::Point3f>& pts3d);
    cv::Mat convertToHomogeneous(const std::vector<cv::Point2f>& pts2d);
    void convertFromHomogeneous(const cv::Mat& hpts3d, std::vector<cv::Point3f>& pts3d);
    void convertFromHomogeneous(const cv::Mat& hpts2d, std::vector<cv::Point2f>& pts2d);
    void convertMatToVecPoint(const cv::Mat& p3d, std::vector<cv::Point3f>& pts3d);
    void convertMatToVecPoint(const cv::Mat& p2d, std::vector<cv::Point2f>& pts2d);
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for reading databases
    ////////////////////////////////////////////////////
    void getImagesFromDataset(const std::string datasetPath, std::vector<std::vector<cv::Mat> >& imageSet);
    bool getImagesFromDataset(const std::string datasetPath, std::vector<std::vector<cv::Mat> >& imageSet,  const int numCams, const int numImgs, const std::string format, int flags);
    
    ////////////////////////////////////////////////////
    /////// Free functions for Saving
    ////////////////////////////////////////////////////
    
    void saveFeatures(const std::string& filename, const ImageFeatures& imgst);
    
    ImageFeatures loadFeatures(const std::string& filename);
    
    
    void saveCameraParams(const std::string& filename, const CameraParams& cp);
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Error Computation
    ////////////////////////////////////////////////////
    
    double computeReprojectionError(const std::vector<cv::Point3f>& objdata, const std::vector<cv::Point2f>& testdata, const CameraParams& cp);
    
    double computeNormalizedCalibrationError(const std::vector<cv::Point2f>& testdata, const CameraParams& cp);
    
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Image Processing
    ////////////////////////////////////////////////////
    
    cv::Mat remap(const cv::Mat& src_img, const CameraParams& cp);
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Multiview geometry
    ////////////////////////////////////////////////////
    
    cv::Mat PfromKRT(const cv::Mat& K, const cv::Mat& R, const cv::Mat& T);
    
    void KRTfromP(const cv::Mat P, cv::Mat& K, cv::Mat& R, cv::Mat& T);
    
    cv::Mat FfromK1K2RT(const cv::Mat& K1, const cv::Mat& K2, const cv::Mat& R, const cv::Mat& T);
    
    void convertToFromskewSymMat(const cv::Mat& src, cv::Mat& dst);
    
    
};//namespace

//NL_UTILS_FEAT_CALIB_H
#endif
