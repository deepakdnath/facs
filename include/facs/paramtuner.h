/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     ParamTuner.h
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#ifndef NL_PARAM_TUNER_H
#define NL_PARAM_TUNER_H

#include "configureFeatCalib.h"

namespace NorthLight {

class ParamTuner{
    public:
        ParamTuner();
        ~ParamTuner();
    
	   std::vector<cv::Mat> addNoise(const std::vector<cv::Mat>& img, float noiseinDB , NoiseType nt);
	   std::vector<cv::Mat> addDistortion(const std::vector<cv::Mat>& img, float Dvalue, DistortionType dt);
	   std::vector<cv::Mat> reSize(const std::vector<cv::Mat>& img, cv::Size size);
	   std::vector<cv::Mat> addBlur(const std::vector<cv::Mat>& img, float factor);


    private:
    

};


};//namespace

//NL_PARAM_TUNER_H
#endif
