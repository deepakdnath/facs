/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     featureextractor.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents:
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#ifndef NL_FEATURE_EXTRACTOR_H
#define NL_FEATURE_EXTRACTOR_H

//#include <string>
//#include <vector>
//#include <opencv2/opencv.hpp>
//#include <timeutils.h>
//#include <opencv2/opencv.hpp>

#include "utilsFeatCalib.h"

namespace NorthLight {


/** Class definition
 */
class FeatureExtractor{
public:
    /** Constructor
     */
    FeatureExtractor(FeatureDetectorType ft);
    FeatureExtractor(FeatureDetectorType ft, cv::Size pattern);
    FeatureExtractor(FeatureDetectorType ft, FeatureMatcherType mt);
   /* FeatureExtractor(FeatureDetectorType ft, FeatureMatcherType mt, cv::Size pattern);*/

    /** Destructor
     */
    ~FeatureExtractor();

    ImageFeatures N_featureDetect(const std::vector<cv::Mat>& vec_src_img);
//    ImageFeatures N_featureDetect(std::vector<IplImage>* vec_src_img);

    FeatureDetectorType getFeatureDetectorType(){return(fType);}

    FeatureMatcherType getFeatureMatcherType(){return(mType);}

    cv::Size getPatternSize(){return(patternSize);}

private:

    FeatureDetectorType fType;
    FeatureMatcherType mType;

    cv::Size patternSize;

    std::string getDetectorStr();
    std::string getMatcherStr();

    void fillFeaturesStruct(ImageFeatures& fstruct, const std::vector<std::vector<cv::KeyPoint> >& multikeypoints,
            const std::vector<cv::Point3f>& objpoints, const  bool validity, const  struct featuresSpeed& fspeed, const std::vector<cv::Vec2i> matchIdxs);

  /** Feature detection given single image
     * \retval returns TRUE if the features are detected and loaded into the relevant structures
     */
//    bool featureDetect(IplImage* src_img, CvMat* image_points, int* tot_feat);
//    ImageFeatures featureDetect(IplImage* src_img);
    ImageFeatures featureDetect(const cv::Mat& src_img);
    ImageFeatures featureDetect(const IplImage* src_img);


    /** Feature detection given stereo images
     * \retval returns TRUE if the features are detected in both images and loaded into the relevant structures
     */
//    bool featureDetect(IplImage* left_img, IplImage* right_img, CvMat* left_image_points, CvMat* right_image_points, int* tot_feat);
    ImageFeatures featureDetect(const cv::Mat& left_img, const cv::Mat& right_img);
    ImageFeatures featureDetect(const IplImage* left_img, const IplImage* right_img);

    /** Feature detection given three images
     * \retval returns TRUE if the features are detected in both images and loaded into the relevant structures
     */
    ImageFeatures featureDetect(const cv::Mat& img1, const cv::Mat& img2, const cv::Mat& img3);
    ImageFeatures featureDetect(const IplImage* img1, const IplImage* img2, const IplImage* img3);


    ImageFeatures featureDetect(const std::vector<cv::Mat>& vec_src_img);

    void featureMatch(const cv::Mat& descriptor_1, const cv::Mat& descriptor_2, std::vector<cv::DMatch>& good_matches);

    void siftDetector(const cv::Mat& src_img, std::vector<cv::KeyPoint>& keypoints);
    void surfDetector(const cv::Mat& src_img, std::vector<cv::KeyPoint>& keypoints);
    void starDetector(const cv::Mat& src_img, std::vector<cv::KeyPoint>& keypoints);
    void orbDetector(const cv::Mat& src_img, std::vector<cv::KeyPoint>& keypoints);
    void siftExtractor(const cv::Mat& src_img,  std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptor );
    void surfExtractor(const cv::Mat& src_img,  std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptor );
    void orbExtractor(const cv::Mat& src_img,  std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptor );
    void bruteMatcher(const cv::Mat& descriptor_1, const cv::Mat& descriptor_2, std::vector<cv::DMatch>& matches);
    void bruteHammingMatcher(const cv::Mat& descriptor_1, const cv::Mat& descriptor_2, std::vector<cv::DMatch>& matches);
    void flannMatcher(const cv::Mat& descriptor_1, const cv::Mat& descriptor_2, std::vector<cv::DMatch>& matches);
};//class FeatureExtractor


}; //namespace

//NL_FEATURE_EXTRACTOR_H
#endif
