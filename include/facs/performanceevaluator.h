/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     PerformanceEvaluator.h
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#ifndef NL_PERFORMANCE_EVALUATOR_H
#define NL_PERFORMANCE_EVALUATOR_H

#include "configureFeatCalib.h"
#include "utilsFeatCalib.h"

namespace NorthLight {

class PerformanceEvaluator{
    public:
        PerformanceEvaluator();
        ~PerformanceEvaluator();
    
		bool epipolarConstraintError(const cv::Mat& funmat, const std::vector<cv::KeyPoint>& keypoints1, const std::vector<cv::KeyPoint>& keypoints2, double &epiError);
		bool epipolarConstraintError(const cv::Mat& funmat, const std::vector<cv::Point2f>& pts1, const std::vector<cv::Point2f>& pts2, double &epiError);
	
		
    private:
    

};


};//namespace

//NL_PERFORMANCE_EVALUATOR_H
#endif