/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     timeutils.h
 * Author:   Alexander Eichhorn <echa@ulrik.uio.no>
 * Contents: Time handling helpers
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */
#ifndef _NL_TIME_UTILS_H
#define _NL_TIME_UTILS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <ostream>

extern const int64_t BILLION;
extern const int64_t MILLION;

namespace NorthLight {

// platform independent clock function returning current time in nsec resolution
struct timespec getTime();

// compares two nanosecond timestamps, the second plus an offset in milliseconds
// to determine whether the first is larger than the second + offset
// (used by LockedQueue to determine over-aged entries)
bool isT1largerThanT2PlusOfsInSec(struct timespec ts1, struct timespec ts2,
                                  uint32_t ts2_offset_in_sec);
bool isT1largerThanT2PlusOfsInMSec(struct timespec ts1, struct timespec ts2,
                                   uint32_t ts2_offset_in_msec);
bool isT1largerThanT2PlusOfsInUSec(struct timespec ts1, struct timespec ts2,
                                   uint32_t ts2_offset_in_usec);

struct timespec ts_sub(struct timespec ts1, struct timespec ts2);
struct timespec ts_add(struct timespec ts1, struct timespec ts2);

struct timespec ts_add(struct timespec ts1, uint64_t nsec);
struct timespec ts_sub(struct timespec ts1, uint64_t nsec);

} // namespace

//---------------------------------------------------------------------------
// struct timespec operators
//---------------------------------------------------------------------------

// outside NorthLight namespace to avoid lookup problems when used with STL

// Arithmetic Operators
inline struct timespec operator +(const struct timespec& ts1,
                                  const uint64_t& nsec)
{
    struct timespec res;
    res.tv_sec = ts1.tv_sec + (ts1.tv_nsec + nsec) / BILLION;
    res.tv_nsec = (ts1.tv_nsec + nsec) % BILLION;
    return res;
}

inline struct timespec operator -(const struct timespec& ts1,
                                  const uint64_t& nsec)
{
    uint64_t tmp = (uint64_t)ts1.tv_sec * BILLION + ts1.tv_nsec - nsec;
    struct timespec res;
    res.tv_sec = tmp / BILLION;
    res.tv_nsec = tmp % BILLION;
    return res;
}

inline struct timespec operator +(const struct timespec& ts1,
                                  const struct timespec& ts2)
{
    return ts1 + ((uint64_t)ts2.tv_sec * BILLION + ts2.tv_nsec);
}

inline struct timespec operator -(const struct timespec& ts1,
                           const struct timespec& ts2)
{
    return ts1 - ((uint64_t)ts2.tv_sec * BILLION + ts2.tv_nsec);
}

inline struct timespec& operator +=(struct timespec& ts, const uint64_t& nsec)
{
    ts.tv_sec = ts.tv_sec + (ts.tv_nsec + nsec) / BILLION;
    ts.tv_nsec = (ts.tv_nsec + nsec) % BILLION;
    return ts;
}

inline struct timespec& operator -=(struct timespec& ts, const uint64_t& nsec)
{
    uint64_t tmp = (uint64_t)ts.tv_sec * BILLION + ts.tv_nsec - nsec;
    ts.tv_sec = tmp / BILLION;
    ts.tv_nsec = tmp % BILLION;
    return ts;
}

inline struct timespec operator *(const struct timespec& ts,
                                  const unsigned int& times)
{
    uint64_t tmp = (uint64_t)ts.tv_sec * BILLION + ts.tv_nsec;
    tmp *= times;
    struct timespec ret;
    ret.tv_sec = tmp / BILLION;
    ret.tv_nsec = tmp % BILLION;
    return ret;
}

inline struct timespec operator /(const struct timespec& ts1,
                                  const unsigned int& times)
{
    uint64_t tmp1 = (uint64_t)ts1.tv_sec * BILLION + ts1.tv_nsec;
    uint64_t div = tmp1 / times;
    uint64_t mod = tmp1 % times;
    struct timespec ret;
    ret.tv_sec = div;
    ret.tv_nsec = mod;
    return ret;
}

inline struct timespec operator /(const struct timespec& ts1,
                                  const struct timespec& ts2)
{
    uint64_t tmp1 = (uint64_t)ts1.tv_sec * BILLION + ts1.tv_nsec;
    uint64_t tmp2 = (uint64_t)ts2.tv_sec * BILLION + ts2.tv_nsec;
    uint64_t div = tmp1 / tmp2;
    uint64_t mod = tmp1 % tmp2;
    struct timespec ret;
    ret.tv_sec = div;
    ret.tv_nsec = mod;
    return ret;
}


// Free Comparison operators
inline bool operator==(const struct timespec& lhs, const struct timespec& rhs)
{
    return     lhs.tv_sec == rhs.tv_sec
            && lhs.tv_nsec == rhs.tv_nsec;
}

inline bool operator==(const struct timespec& lhs, const uint64_t& rhs)
{
    uint64_t tmp = (uint64_t)lhs.tv_sec * BILLION + lhs.tv_nsec;
    return tmp == rhs;
}


inline bool operator!=(const struct timespec& lhs, const struct timespec& rhs)
{
    return     lhs.tv_sec != rhs.tv_sec
            || lhs.tv_nsec != rhs.tv_nsec;
}

inline bool operator!=(const struct timespec& lhs, const uint64_t& rhs)
{
    uint64_t tmp = (uint64_t)lhs.tv_sec * BILLION + lhs.tv_nsec;
    return tmp != rhs;
}


inline bool operator>(const struct timespec& lhs, const struct timespec& rhs)
{
    return (lhs.tv_sec > rhs.tv_sec) ||
        (lhs.tv_sec == rhs.tv_sec && lhs.tv_nsec > rhs.tv_nsec);
}

inline bool operator>(const struct timespec& lhs, const uint64_t& rhs)
{
    uint64_t tmp = (uint64_t)lhs.tv_sec * BILLION + lhs.tv_nsec;
    return tmp > rhs;
}


inline bool operator>=(const struct timespec& lhs, const struct timespec& rhs)
{
    return (lhs > rhs) || (lhs == rhs);
}


inline bool operator<(const struct timespec& lhs, const struct timespec& rhs)
{
    return (lhs.tv_sec < rhs.tv_sec) ||
        (lhs.tv_sec == rhs.tv_sec && lhs.tv_nsec < rhs.tv_nsec);
}

inline bool operator<(const struct timespec& lhs, const uint64_t& rhs)
{
    uint64_t tmp = (uint64_t)lhs.tv_sec * BILLION + lhs.tv_nsec;
    return tmp < rhs;
}

inline bool operator<=(const struct timespec& lhs, const struct timespec& rhs)
{
    return (lhs < rhs) || (lhs == rhs);
}

/*inline struct timespec operator float() const
{
    return (float)tv_sec * BILLION + (float)tv_nsec;
}

inline struct timespec operator uint64_t(const & ts) const
{
    return (uint64_t)tv_sec * BILLION + (uint64_t)tv_nsec;
}
*/
inline std::ostream& operator<< (std::ostream& str, const struct timespec& t)
{
    str << t.tv_sec << ":";
    int width = str.width (9);
    std::ostream::char_type ch = str.fill ('0');
    str << t.tv_nsec;
    str.width (width);
    str.fill (ch);
    return str;
}

#endif // _NL_TIMEUTILS_H

