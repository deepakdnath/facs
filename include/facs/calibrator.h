/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     calibrator.h
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

/*
 * TODO
 * i dont need CalibMethod until i use variety of markerless methods.
 * currently I have 3d to 2D, adn given 3D, 2D, 2D for stereocalibrate.
 */

#ifndef NL_CALIBRATOR_H
#define NL_CALIBRATOR_H

#include "utilsFeatCalib.h"
//#include <multicampoints.h>

namespace NorthLight {



/** Class definition
 */
class Calibrator{
public:
    /** Constructor
     */
    Calibrator(IntrinsicMethod im);
    Calibrator(FunMatMethod fm, PoseMethod pm);
    Calibrator(FunMatMethod fm, IntrinsicMethod im, PoseMethod pm);
//    Calibrator(CalibMethod ct, cv::Size imgsize);

    /** Destructor
     */
    ~Calibrator();
/*
    bool N_calibrate(CalibFeatures& cf,
                     const cv::Size imgSize,
                     const std::vector<std::vector<std::vector<cv::Point2f> > >& imgpoints,
                     const std::vector<std::vector<std::vector<cv::Point3f> > >& objpoints,
                     const std::vector<cv::Mat>& Ks,
                     const std::vector<cv::Mat>& Ds);
*/
    bool N_calibrate(CalibFeatures& cf,
                     const cv::Size imgSize,
                     MultiCamPoints* multicampts,
                     const std::vector<cv::Mat>& Ks,
                     const std::vector<cv::Mat>& Ds);


    /** Calibrate given three image keypoints
     */
//    bool calibN(std::vector<cv::Point2f>& imgpts1, std::vector<cv::Point2f>& imgpts2, std::vector<cv::Point2f>& imgpts3, CalibFeatures& cf);

    FunMatMethod getFunMatMethod(){return(fMeth);}

    IntrinsicMethod getIntrinsicMethod(){return(iMeth);}

    PoseMethod getPoseMethod(){return(pMeth);}

private:
    FunMatMethod fMeth;
    PoseMethod pMeth;
    IntrinsicMethod iMeth;

    bool known3D;
    bool knownIntrinsics;

   // uint32_t numCams;
   // uint32_t numTrainSize;
   // uint32_t numMatchPoints;

    cv::Size imageSize;

    //intrinsics only
    bool calibIntrinsics(CalibFeatures& cf,
                             const std::vector<std::vector<cv::Point2f> >& imgpts,
                             const std::vector<std::vector<cv::Point3f> >& objpts);



    //extrinsics wiht known 3D adn unknown/partially known parameters
    bool calibCameraPose3Dto2D(CalibFeatures& cf,
                               const std::vector<std::vector<cv::Point2f> >& imgpts1,
                               const std::vector<std::vector<cv::Point2f> >& imgpts2,
                               const std::vector<std::vector<cv::Point3f> >& objpts,
                               const std::vector<cv::Mat>& Ks,
                               const std::vector<cv::Mat>& Ds);

    //extrinsics with unknown 3D partially known parameters
    bool calibCameraPose2Dto2D(CalibFeatures& cf,
                               const std::vector<cv::Point2f>& imgpts1,
                               const std::vector<cv::Point2f>& imgpts2,
                               const std::vector<cv::Mat>& Ks,
                               const std::vector<cv::Mat>& Ds);


    //auto with unknown scene, unknown parameters
    bool calibAuto(CalibFeatures& cf,
                               const std::vector<cv::Point2f>& imgpts1,
                               const std::vector<cv::Point2f>& imgpts2);





    cv::Mat estimateFunMat(   const std::vector<cv::Point2f>& imgpts1,
                               const std::vector<cv::Point2f>& imgpts2);


    bool recoverRT_8pointalgorithm(const std::vector<cv::Point2f>& imgpts1,
                                   const std::vector<cv::Point2f>& imgpts2,
                                   const cv::Mat& K1, const cv::Mat& D1,
                                   const cv::Mat& K2, const cv::Mat& D2,
                                   cv::Mat& R, cv::Mat& T,
                                   cv::Mat& E, cv::Mat& F);

//    void convertVecPointToMat(const std::vector<cv::Point3f>& pts3d, cv::Mat& outMat);



    cv::Mat triangulationLinear(const cv::Mat& P1, const cv::Mat& P2,
                       const std::vector<cv::Point2f>& imgpts1,
                       const std::vector<cv::Point2f>& imgpts2,
                       std::vector<cv::Point3f>& est_objpts);


    void fillCalibStruct(CalibFeatures& cstruct,
                        const std::vector<CameraParams>& multicamparams,
                        const struct calibrationSpeed& cspeed);

};//class Calibrator




}; //namespace

//NL_CALIBRATOR_H
#endif


