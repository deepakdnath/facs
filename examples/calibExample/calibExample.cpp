/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     calibExample.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: Example showing how to use the Camera Calibration Framework
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#include <iostream>
#include <getopt.h>

//#include <calibrator.h>
//#include <featureextractor.h>
#include "facs/libfacs.h"

using namespace std;
using namespace cv;
using namespace NorthLight;


////////////////////////////////
struct AppConfig{
    FunMatMethod fm;
    PoseMethod pm;
    IntrinsicMethod im;
    int calibType;
    AppConfig(){};
    ~AppConfig(){};
};

void usage(const char* progname)
{
    cout<< "Usage: "<< progname << " [Options] traingingset 1 trainingset 2...\n"
        "[trainingset N] can have arbitrary number of images, but same for all cameras ...\n"

        "[Options]...\n"
            "   -c, --calibration  [calibration arg options] Featuretype\n"
            "   -i, --intrinsics  [Intrinsic arg options] IntrinsicMethod\n"
            "   -p, --camerapose  [Camera Pose arg options] PoseMethod\n"
            "   -f, --funmatrix  [Fundamental Matrix arg options] FunMatMethod\n"

        "[Calibration arg options]...\n"
        "   0,   none calibrate\n"
        "   1,   single calibrate\n"
        "   2,   stereo calibrate [3d known]\n"
        "   3,   stereo calibrate [3d unknown]\n"

        "[Intrinsic arg options]...\n"
        "   0,   none method\n"
        "   1,   bouguet method [ONLY FOR 3D KNOWN PLANAR POINTS]\n"
        "   2,   tsai method [ONLY FOR 3D KNOWN PLANAR/NON_PLANAR POINTS]\n"
        "   3,   heikkila method [ONLY FOR 3D KNOWN PLANAR/NON-PLANAR POINTS]\n"
        "   4,   zhang method [ONLY FOR 3D KNOWN PLANAR POINTS]\n"

        "[Camera Pose arg options]...\n"
        "   0,   none method\n"
        "   1,   bouguet method [ONLY FOR 3D KNOWN PLANAR/NON-PLANAR POINTS]\n"
        "   2,   8point method [ONLY FOR 2D POINTS and GIVEN INTRINSICS]\n"
        "   3,   8point + bundle adjustment method [ONLY FOR 2D POINTS and GIVEN INTRINSICS]\n"

        "[Fundamental Matrix arg options]...\n"
        "   0,   none method\n"
        "   1,   7 point method \n"
        "   2,   8 point method \n"
        "   3,   RANSAC method \n"
        "   4,   LMEDS method \n"
        "   5,   5 point method \n"

        "[Help options]...\n"
        "   -h, --help   usage\n";
}


void parseCmdLine(int argc, char* argv[], AppConfig& cfg)
{
    char c;
      int long_options_index = -1;

    static struct option long_options[] =
        {
            {"calibrate", required_argument, NULL, 'c'},
            {"intrinsics", required_argument, NULL, 'i'},
            {"camerapose", required_argument, NULL, 'p'},
            {"funmatrix", required_argument, NULL, 'f'},
            {"help", no_argument, NULL, 'h'},
            {0, 0, 0, 0}
        };

    while ((c = getopt_long (argc, argv, "c:i:p:f:hH", long_options, &long_options_index)) != -1)
 //   while ((c = getopt(argc, argv, "c:hH")) != -1)
    {
        switch (c)
        {
        case 'c':
            cfg.calibType = (int)atoi(optarg);
            break;
        case 'i':
            cfg.im = (IntrinsicMethod)atoi(optarg);
            break;
        case 'p':
            cfg.pm = (PoseMethod)atoi(optarg);
            break;
        case 'f':
            cfg.fm = (FunMatMethod)atoi(optarg);
            break;
        case 'H': // wrong option
        case 'h': // wrong option
        case '?': // wrong option
            usage(argv[0]);
            exit(1);
            break;
        default:
            cout << "Error: Wrong argument " << argv[optind] << endl;
            usage(argv[0]);
            exit(1);
            break;
        };
    }
}




/////////////////////////////////////
int main(int argc, char* argv[]){

   cout << "---->>> calibExampleApp Example." << endl;
  // cout << argc;
   if(argc<10){
      cout << "Error: Wrong argument " << argv[optind] << endl;
      usage(argv[0]);
      exit(1);
    }
   AppConfig cfg;
   parseCmdLine(argc, argv, cfg);



  // read images
   vector<Mat> img;
   for(int c=9,i=c-9;c<argc;c++,i++){
       if(argv[c]){
           img.push_back(imread(argv[c]));
         if(!img[i].data){cout << "Could not Load the image " << i << endl;return 0;}
        else{cout << "Image " << i << " of size " << img[i].size().width << "x" << img[i].size().height <<" loaded SUCCESSFULLY\n" << endl;}
         }
    }


    vector<vector<Point3f> > opt, opt2;
    vector<vector<Point2f> > ipt, ipt2;
    vector<Point2f> tipt;
    vector<Point2f> tipt2;
    vector<Mat> matimg, Ks, Ds;


    FeatureExtractor* m_fe;
    ImageFeatures imgst;

    Calibrator* m_ca;
    m_ca = new Calibrator(cfg.fm, cfg.im, cfg.pm);
    CalibFeatures calibst;
    MultiCamPoints* m_mcp;

    bool res = false;
    uint32_t  numTraingset;
    uint32_t  trIdx;

    //dummy intrinsics
    double K [3][3]=
    {
        {1000, 0, 250},
        {0, 1000, 240},
        {0, 0, 1}
    };

    double D [5][1]=
    {
        {-2.82088183103143e-15},
        {-1.215332910111713e-30},
        {2.044410855342281e-08},
        {3.648748839656825e-07},
        {-5.650875718614387e-46}
    };
    Mat k = Mat(3,3,CV_64F, K);
    Mat d = Mat(1,5,CV_64F, D);

   
    switch(cfg.calibType){

        case 1://single image, then treat all inputs as training set;
            m_fe = new FeatureExtractor(POINT_FEATURE_TYPE_CORNER, Size(9,6));
            m_mcp = new MultiCamPoints(1);

            //load feature points into the container for calibration
            for(uint32_t i=0; i<img.size();i++){
                matimg.clear();
                matimg.push_back(img[i]);
                imgst = m_fe->N_featureDetect(matimg);
                if(imgst.isValid){
                    m_mcp->add3DPoints(imgst.objpoints);
                    KeyPoint::convert(imgst.multikeypoints[0], tipt);
                    m_mcp->add2DPoints(tipt,0);
                    if(m_mcp->checkConsistency()){
                        res = m_ca->N_calibrate(calibst, img[0].size(), m_mcp, Ks, Ds);
                    }
                }else{cout << "feature detection FAILED" << endl;}
            }

            cout << "-------OPERATION ON " << img.size() << " image/s----------\n" << endl;
            if(res){cout << "--- Operation: SUCCESSFUL" << endl;}
            else{cout << "--- Operation: FAILED" << endl; return 0;}


            cout << "-----Calibration Parameters" << endl;
            cout << "---Camera Matrix\n" << calibst.multicamparams[0].K << endl;
            cout << "---Distortion Coeffs\n" << calibst.multicamparams[0].D << endl;
         //   cout << "---Rotation Matrix\n" << calibst.multicamparams[0].R << endl;
         //   cout << "---Translation Matrix\n" << calibst.multicamparams[0].T << endl;
         //   cout << "---Fundamental Matrix\n" << calibst.multicamparams[0].F << endl;
         //   cout << "---Essential Matrix\n" << calibst.multicamparams[0].E << endl;

            cout << "---Time " << (calibst.cspeed.calibrationTime.tv_sec * MILLION + calibst.cspeed.calibrationTime.tv_nsec/1000) << endl;

            break;

        case 2:
				 //dummy
			Ks.push_back(k);
			Ds.push_back(d);
			Ks.push_back(k);
			Ds.push_back(d);
		
            m_fe = new FeatureExtractor(POINT_FEATURE_TYPE_CORNER, Size(9,6));
            m_mcp = new MultiCamPoints(2);

            trIdx = img.size()%2;
            numTraingset = img.size()/2;
            if(trIdx!=0){cout << "training set are of different sizes - provide equal number of images" << endl;return 0;}
            //load feature points into the container for calibration
             for(uint32_t i=0; i<numTraingset;i++){
                    matimg.clear();
                    matimg.push_back(img[i]);
                    matimg.push_back(img[i+numTraingset]);
                    imgst = m_fe->N_featureDetect(matimg);
                    if(imgst.isValid){
                        m_mcp->add3DPoints(imgst.objpoints);
                        KeyPoint::convert(imgst.multikeypoints[0], tipt);
                        KeyPoint::convert(imgst.multikeypoints[1], tipt2);
                        m_mcp->add2DPoints(tipt,0);
                        m_mcp->add2DPoints(tipt2,1);
                        if(m_mcp->checkConsistency()){
                            res = m_ca->N_calibrate(calibst, img[0].size(), m_mcp, Ks, Ds);
                        }
                    }else{cout << "feature detection FAILED" << endl;}
             }


            cout << "-------OPERATION ON " << img.size() << " image/s----------\n" << endl;
            if(res){cout << "--- Operation: SUCCESSFUL" << endl;}
            else{cout << "--- Operation: FAILED" << endl; return 0;}


            cout << "-----Calibration Parameters" << endl;
            cout << "---Camera Matrix 1\n" << calibst.multicamparams[0].K << endl;
            cout << "---Distortion Coeffs 1\n" <<  calibst.multicamparams[0].D << endl;
            cout << "---Rotation Matrix 1\n" <<  calibst.multicamparams[0].R << endl;
            cout << "---Translation Matrix 1\n" <<  calibst.multicamparams[0].T << endl;
            cout << "---Camera Matrix 2\n" << calibst.multicamparams[1].K << endl;
            cout << "---Distortion Coeffs 2\n" <<  calibst.multicamparams[1].D << endl;
            cout << "---Rotation Matrix 2\n" <<  calibst.multicamparams[1].R << endl;
            cout << "---Translation Matrix 2\n" <<  calibst.multicamparams[1].T << endl;
            cout << "---Fundamental Matrix\n" << calibst.multicamparams[1].F << endl;
            cout << "---Essential Matrix\n" << calibst.multicamparams[1].E << endl;


            cout << "---Time " << (calibst.cspeed.calibrationTime.tv_sec * MILLION + calibst.cspeed.calibrationTime.tv_nsec/1000) << endl;

            break;

		case 3:
			 //dummy
			Ks.push_back(k);
			Ds.push_back(d);
			Ks.push_back(k);
			Ds.push_back(d);
		
            m_fe = new FeatureExtractor(POINT_FEATURE_TYPE_SIFT, MATCHER_TYPE_FLANN);
            m_mcp = new MultiCamPoints(2);

            trIdx = img.size()%2;
            numTraingset = img.size()/2;
            if(trIdx!=0){cout << "training set are of different sizes - provide equal number of images" << endl;return 0;}
            //load feature points into the container for calibration
             for(uint32_t i=0; i<numTraingset;i++){
                    matimg.clear();
                    matimg.push_back(img[i]);
                    matimg.push_back(img[i+numTraingset]);
                    imgst = m_fe->N_featureDetect(matimg);
                    if(imgst.isValid){
                           KeyPoint::convert(imgst.multikeypoints[0], tipt);
                           KeyPoint::convert(imgst.multikeypoints[1], tipt2);
                           m_mcp->add2DPoints(tipt,0);
                           m_mcp->add2DPoints(tipt2,1);

                           if(m_mcp->checkConsistency()){
                             res = m_ca->N_calibrate(calibst, img[0].size(), m_mcp, Ks, Ds);
                           }
                        }else{cout << "feature detection FAILED" << endl;}             
             }


            cout << "-------OPERATION ON " << img.size() << " image/s----------\n" << endl;
            if(res){cout << "--- Operation: SUCCESSFUL" << endl;}
            else{cout << "--- Operation: FAILED" << endl; return 0;}


            cout << "-----Calibration Parameters" << endl;
            cout << "---Camera Matrix 1\n" << calibst.multicamparams[0].K << endl;
            cout << "---Distortion Coeffs 1\n" <<  calibst.multicamparams[0].D << endl;
            cout << "---Rotation Matrix 1\n" <<  calibst.multicamparams[0].R << endl;
            cout << "---Translation Matrix 1\n" <<  calibst.multicamparams[0].T << endl;
            cout << "---Camera Matrix 2\n" << calibst.multicamparams[1].K << endl;
            cout << "---Distortion Coeffs 2\n" <<  calibst.multicamparams[1].D << endl;
            cout << "---Rotation Matrix 2\n" <<  calibst.multicamparams[1].R << endl;
            cout << "---Translation Matrix 2\n" <<  calibst.multicamparams[1].T << endl;
            cout << "---Fundamental Matrix\n" << calibst.multicamparams[1].F << endl;
            cout << "---Essential Matrix\n" << calibst.multicamparams[1].E << endl;


            cout << "---Time " << (calibst.cspeed.calibrationTime.tv_sec * MILLION + calibst.cspeed.calibrationTime.tv_nsec/1000) << endl;
			break;
        default:
            break;
    }//switch

}//main

