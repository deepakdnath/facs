/*
 ============================================================================
 Name        : exampleProgram.c
 Author      : Deepak Dwarakanath (deepakd@simula.no)
 Version     :
 Copyright   : This software is released under the terms of the GNU Lesser General Public License Version 2 (LGPLv2)
 Description : Uses shared library to print greeting
               To run the resulting executable the LD_LIBRARY_PATH must be
               set to ${project_loc}/libfacs/.libs
               Alternatively, libtool creates a wrapper shell script in the
               build directory of this program which can be used to run it.
               Here the script will be called exampleProgram.
 ============================================================================
 */

//#include "libfacs.h"

int main(void) {
 // print_hello();
  return 0;
}
