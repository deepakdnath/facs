/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     featExample.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: Example for Feature Extraction
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#include <iostream>
#include <getopt.h>

//#include <featureextractor.h>
#include "facs/libfacs.h"


using namespace std;
using namespace cv;
using namespace NorthLight;


////////////////////////////////
struct AppConfig{
    FeatureDetectorType ft;
    FeatureMatcherType mt;
    AppConfig(){};
    ~AppConfig(){};
};

void usage(const char* progname)
{
    cout<< "Usage: "<< progname << " [Options] images ...(1 /2 /3))\n"
        "[Options]...\n"
            "   -f, --feature  [Feature arg options] Featuretype\n"
            "   -m, --matcher  [Matcher arg options] Matchertype\n"


        "[Feature arg options]...\n"
        "   0,    none features\n"
        "   1,    corner features\n"
        "   2,    sift features\n"
        "   3,    surf features\n"

        "[Match arg options]...\n"
        "   0,    none matcher\n"
        "   1,    bruteforce matcher\n"
        "   2,    bruteforce-l2 matcher\n"
        "   3,    bruteforce-hamming matcher\n"
        "   4,    bruteforce-hamminglut matcher\n"
        "   5,    flannbased matcher\n"

        "[Help options]...\n"
        "   -h, --help   usage\n";
}


void parseCmdLine(int argc, char* argv[], AppConfig& cfg)
{
    char c;
      int long_options_index = -1;

    static struct option long_options[] =
        {
            {"feature", required_argument, NULL, 'f'},
            {"matcher", required_argument, NULL, 'm'},
            {"help", no_argument, NULL, 'h'},
            {0, 0, 0, 0}
        };

    while ((c = getopt_long (argc, argv, "f:m:hH", long_options, &long_options_index)) != -1)
 //   while ((c = getopt(argc, argv, "c:hH")) != -1)
    {
        switch (c)
        {
        case 'f':
            cfg.ft = (FeatureDetectorType)atoi(optarg);
            break;
        case 'm':
            cfg.mt = (FeatureMatcherType)atoi(optarg);
            break;
        case 'H': // wrong option
        case 'h': // wrong option
        case '?': // wrong option
            usage(argv[0]);
            exit(1);
            break;
        default:
            cout << "Error: Wrong argument " << argv[optind] << endl;
            usage(argv[0]);
            exit(1);
            break;
        };
    }
}



/////////////////////////////////////
int main(int argc, char* argv[]){

   cout << "---->>> FeatureExtractApp Example." << endl;
   if(argc<6){
      cout << "Error: Wrong argument " << argv[optind] << endl;
      usage(argv[0]);
      exit(1);
    }

   AppConfig cfg;
   parseCmdLine(argc, argv, cfg);


  // read images
   vector<cv::Mat> img;
   for(int c=5,i=c-5;c<argc;c++,i++){
       if(argv[c]){
           img.push_back(imread(argv[c]));
         if(!img[i].data){cout << "Could not Load the image " << i << endl;return 0;}
        else{cout << "Image " << i << " of size " << img[i].size().width << "x" << img[i].size().height <<" loaded SUCCESSFULLY\n" << endl;}
         }
    }

//   vector<cv::Mat>::iterator it;
//  for(it=img.begin();it!=img.end();it++){
//      cout << *it << endl;
        FeatureExtractor* m_fe;
        if(cfg.ft==POINT_FEATURE_TYPE_CORNER){m_fe = new FeatureExtractor(cfg.ft, Size(9,6));}
        else if(img.size()==1){m_fe = new FeatureExtractor(cfg.ft);}
        else{m_fe = new FeatureExtractor(cfg.ft, cfg.mt);}

        ImageFeatures imgst = m_fe->N_featureDetect(img);

//      ImageFeatures imgst = m_fe->featureDetect(img[0]);
//      ImageFeatures imgst = m_fe->featureDetect(*it);

        cout << "-------OPERATION ON " << img.size() << " image/s----------\n" << endl;
        if(imgst.isValid){cout << "--- Operation: SUCCESSFUL" << endl;}
            else{cout << "--- Operation: FAILED due to Features NOT detection" << endl; return 0;}
        switch(imgst.fType){
            case POINT_FEATURE_TYPE_CORNER:
                cout << "--- Feature Detector type: CORNERS" << endl;
                break;
            case POINT_FEATURE_TYPE_SIFT:
                cout << "--- Feature Detector type: SIFT" << endl;
                break;
            case POINT_FEATURE_TYPE_SURF:
                cout << "--- Feature Detector type: SURF" << endl;
                break;
            default:
                cout <<  "--- Feature Detector type: NONE" << endl;
                break;
        }

        switch(imgst.mType){
            case MATCHER_TYPE_BRUTEFORCE:
                cout << "--- Feature Matcher type: BRUTEFORCE" << endl;
                break;
            case MATCHER_TYPE_BRUTEFORCE_L1:
                cout << "--- Feature Matcher type: BRUTEFORCE_L1" << endl;
                break;
            case MATCHER_TYPE_BRUTEFORCE_HAMMING:
                cout << "--- Feature Matcher type: BRUTEFORCE_HAMMING" << endl;
                break;
            case MATCHER_TYPE_BRUTEFORCE_HAMMINGLUT:
                cout << "--- Feature Matcher type: BRUTEFORCE_HAMMINGLUT" << endl;
                break;
            case MATCHER_TYPE_FLANN:
                cout << "--- Feature Matcher type: FLANN" << endl;
                break;
            default:
                cout <<  "--- Feature Matcher type: NONE" << endl;
                break;
        }


        cout << "--- Performance on Speed" << endl;
        if(imgst.fspeed.detectionTime.tv_sec || imgst.fspeed.detectionTime.tv_nsec){
            cout << "------detection: " << (imgst.fspeed.detectionTime.tv_sec * MILLION +  imgst.fspeed.detectionTime.tv_nsec/1000) << " microsecs" << endl;
        }else{
            cout << "------detection: " << "invalid" << endl;
        }
        if(imgst.fspeed.descriptionTime.tv_sec || imgst.fspeed.descriptionTime.tv_nsec){
            cout << "------description: " << (imgst.fspeed.descriptionTime.tv_sec * MILLION +  imgst.fspeed.descriptionTime.tv_nsec/1000) << " microsecs" <<  endl;
        }else{
            cout << "------description: " << "invalid" << endl;
        }
        if(imgst.fspeed.matchingTime.tv_sec || imgst.fspeed.matchingTime.tv_nsec){
            cout << "------matching: " << (imgst.fspeed.matchingTime.tv_sec * MILLION +  imgst.fspeed.matchingTime.tv_nsec/1000) << " microsecs" <<  endl;
        }else{
            cout << "------matching: " << "invalid" << endl;
        }

        Mat imageMatches;
        vector<DMatch> matches;
        DMatch match;
        switch(img.size()){
            case 1:
                cout << "\n---DETECTED POINTS" << endl;
                for (uint32_t i=0;i<imgst.multikeypoints[0].size();i++){
                    if(imgst.fType==POINT_FEATURE_TYPE_CORNER){cout << "ObjPoint" << i+1 << ":" << imgst.objpoints[i] << endl;}
                     cout << "Point" << i+1 << ":" << imgst.multikeypoints[0][i].pt << endl;
                     cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][0] << endl;
                }
                break;

            case 2:
                cout << "---NUMBER OF MATCHES:" << imgst.multikeypoints[0].size() << endl;
                if(imgst.multikeypoints[0].size()){
                    cout << "\n--- MATCHED POINTS" << endl;
                    for (uint32_t i=0;i<imgst.multikeypoints[0].size();i++){
                       match.queryIdx = i;
                       match.trainIdx = i;
                       matches.push_back(match);
  //                for (uint32_t i=0;i<imgst.multikeypoints[0].size()/20;i++){
                        cout << "Point" << i+1 << ":" << imgst.multikeypoints[0][i].pt << endl;
                        cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][0] << endl;
                        cout << "Point" << i+1 << ":" << imgst.multikeypoints[1][i].pt << endl;
                        cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][1] << endl;
                    }
                }

                cv::drawMatches(
                      img[0],imgst.multikeypoints[0], // 1st image and its keypoints
                      img[1],imgst.multikeypoints[1], // 2nd image and its keypoints
                      matches, // the matches
                      imageMatches, // the image produced
                      cv::Scalar(255,255,255)); // color of the lines
                imwrite("matches.tif", imageMatches);
				
				namedWindow("matchingImage");
				imshow("matchingImage", imageMatches);
				waitKey(50000);

                break;

           case 3:
               break;
        }//switch

         cout << "---Testing get functions" << endl;
         cout << "ftype " << (int)m_fe->getFeatureDetectorType() << endl;
         cout << "mtype " << (int)m_fe->getFeatureMatcherType() << endl;
         cout << "patternsize " << m_fe->getPatternSize().width << "x" << m_fe->getPatternSize().height << endl;

/*
///just for trying

            Mat test= Mat::eye(2,3,CV_32F);
//            test.at<double>(1,1)  =1.2;
//            test.reshape(0,3);
            float arr[3]={1, 3, 5};
            Mat r = Mat(2,3, CV_32F, arr);
            cout << r << endl;
            Mat g = Mat(3,3, CV_32F);
            g.push_back(r.row(0));
            cout << "empty" << g << endl;
            cout << "testing" << endl;
//            r.col(0).copyTo(test);
            cout << test << endl;
            g.push_back(r.row(1));
            g.push_back(test.row(1));
            cout << "empty" << g << endl;
*/
    //   saveFeatures("ImageFeatures.txt", imgst);
    //   loadFeatures("ImageFeatures.txt");

    //}

}//main

