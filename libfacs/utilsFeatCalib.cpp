/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     utilsFeatCalib.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#include "facs/utilsFeatCalib.h"
//#include <tinyxml.h>
#include <fstream>
#include <stdio.h>

namespace NorthLight {
    
    using namespace std;
    using namespace cv;
    
    ////////////////////////////////////////////////////
    //////class MultiCamPoints
    ////////////////////////////////////////////////////
    
    MultiCamPoints::MultiCamPoints(const uint8_t a_numCameras)
    :numCameras(a_numCameras){
        vector<Point2f> dummy2f;
        for (int i =0; i< numCameras; i++){
            ncam_imgpts.push_back(dummy2f);
        }
    }
    
    MultiCamPoints::~MultiCamPoints(){
        ncam_imgpts.clear();
    }
    
    
    void MultiCamPoints::add2DPoints(const vector<Point2f>& a_imgpts, const uint8_t& a_numCameras){
        for (int i=0; i< a_imgpts.size(); i++){
            if(ncam_imgpts[a_numCameras].size()< ncam_imgpts[a_numCameras].max_size()){
                ncam_imgpts[a_numCameras].push_back(a_imgpts[i]);
            }
        }
        /*   cout << "after add size of vvimg" << m_mcampts.ncam_imgpts.size() << endl;
         cout << "after add size of img" << m_mcampts.ncam_imgpts[0].size() << endl;
         cout << "after add size of img" << m_mcampts.ncam_imgpts[1].size() << endl;
         
         cout << "size: " << ncam_imgpts[0].size() << "\n";
         cout << "capacity: " << ncam_imgpts[0].capacity() << "\n";
         cout << "max_size: " << ncam_imgpts[0].max_size() << "\n";
         */
    }
    
    bool MultiCamPoints::get2Dpoints(const uint8_t& a_numCameras, vector<Point2f>& a_imgpts) {
        
        if(!ncam_imgpts[a_numCameras].size()){return false;}
        else{a_imgpts = ncam_imgpts[a_numCameras];return true;}
        
    }
    
    void MultiCamPoints::add3DPoints(const vector<Point3f>& a_objpts){
        
        
        for (int i=0; i< a_objpts.size(); i++){
            if(ncam_objpts.size()< ncam_objpts.max_size()){
                ncam_objpts.push_back(a_objpts[i]);
            }
        }
    }
    
    
    bool MultiCamPoints::get3Dpoints(vector<Point3f>& a_objpts){
        
        if(!ncam_objpts.size()){return false;}
        else{a_objpts = ncam_objpts;return true;}
    }
    
    
    bool MultiCamPoints::checkConsistency(){
        bool retval = true;
        int ip[numCameras];
        int ips=0;
        int err=0;
        
        for(int i=0; i<numCameras; i++){
            if(ncam_imgpts[i].size()==0){
                cout << "ERROR in MultiCamPoints inconsistency: Data for Image " << i << " not available" << endl;
                err=1;
            }
        }
        if(err){retval = false;return retval;}
        
        for(int i=0; i<numCameras; i++){
            ip[i] = ncam_imgpts[i].size();
            if(i==0){ips=ip[i];}
            if(ip[i]!=ips){
                cout << "ERROR in MultiCamPoints inconsistency: Image points across different cameras does not match" << endl;
                retval=false;
            }  //check for equal dimension and size not zero.
        }
        
        if(ncam_objpts.size() && (ips!=ncam_objpts.size())){
            cout << "ERROR in MultiCamPoints inconsistency: object points and image points are of different sizes" << endl;
            retval = false;
        }
        
        return retval;
    }
    
    
    uint8_t MultiCamPoints::getNumCameras(){
        return numCameras;
    }
    
    
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Conversions
    ////////////////////////////////////////////////////
    Mat convertToHomogeneous(const vector<Point3f>& pts3d){
        Mat hpts3d = Mat(4, pts3d.size(), CV_64F);
        //  Mat p3d = convertVecPointToMat(pts3d);
        
        for (int i=0; i<pts3d.size();i++){
            hpts3d.at<double>(0,i) = pts3d[i].x;
            hpts3d.at<double>(1,i) = pts3d[i].y;
            hpts3d.at<double>(2,i) = pts3d[i].z;
            hpts3d.at<double>(3,i) = 1.0;
        }
        return hpts3d;
    }
    
    Mat convertToHomogeneous(const vector<Point2f>& pts2d){
        Mat hpts2d = Mat(3, pts2d.size(), CV_64F);
        //  Mat p2d = convertVecPointToMat(pts2d);
        
        for (int i=0; i<pts2d.size();i++){
            hpts2d.at<double>(0,i) = pts2d[i].x;
            hpts2d.at<double>(1,i) = pts2d[i].y;
            hpts2d.at<double>(2,i) = 1.0;
        }
        return hpts2d;
    }
    
    
    void convertFromHomogeneous(const Mat& hpts3d, vector<Point3f>& pts3d){
        
        if(hpts3d.rows !=4){cout << "ERROR in convertFromHomogeneous: input matrix dim is != 4" << endl;}
        Point3f tp;
        for (int i=0; i<hpts3d.cols;i++){
            tp.x = hpts3d.at<double>(0,i)/hpts3d.at<double>(3,i);
            tp.y = hpts3d.at<double>(1,i)/hpts3d.at<double>(3,i);
            tp.z = hpts3d.at<double>(2,i)/hpts3d.at<double>(3,i);
            pts3d.push_back(tp);
        }
    }
    
    void convertFromHomogeneous(const Mat& hpts2d, vector<Point2f>& pts2d){
        
        if(hpts2d.rows !=3){cout << "ERROR in convertFromHomogeneous: input matrix dim is != 3" << endl;}
        Point2f tp;
        for (int i=0; i<hpts2d.cols;i++){
            tp.x = hpts2d.at<double>(0,i)/hpts2d.at<double>(2,i);
            tp.y = hpts2d.at<double>(1,i)/hpts2d.at<double>(2,i);
            pts2d.push_back(tp);
        }
    }
    
    
    Mat convertVecPointToMat(const vector<Point3f>& pts3d){//, Mat& outMat){
        
        int numpts = pts3d.size();
        Mat outMat = Mat(3, numpts, CV_64F);
        double arr[3][numpts];
        
        for(int i=0;i<numpts;i++){
            outMat.at<double>(0,i) = pts3d[i].x;
            outMat.at<double>(1,i) = pts3d[i].y;
            outMat.at<double>(2,i) = pts3d[i].z;
        }
        return outMat;
    }
    
    Mat convertVecPointToMat(const vector<Point2f>& pts2d){//, Mat& outMat){
        
        int numpts = pts2d.size();
        Mat outMat = Mat(2, numpts, CV_64F);
        double arr[2][numpts];
        
        for(int i=0;i<numpts;i++){
            outMat.at<double>(0,i) = pts2d[i].x;
            outMat.at<double>(1,i) = pts2d[i].y;
        }
        return outMat;
    }
    
    void convertMatToVecPoint(const Mat& p3d, vector<Point3f>& pts3d){
        
        if(p3d.rows !=3){cout << "ERROR in convertMatToVecPoint: input matrix dim is != 3" << endl;}
        Point3f tp;
        for(int i=0;i<p3d.cols;i++){
            tp.x = p3d.at<double>(0,i);
            tp.y = p3d.at<double>(1,i);
            tp.z = p3d.at<double>(2,i);
            pts3d.push_back(tp);
        }
    }
    
    void convertMatToVecPoint(const Mat& p2d, vector<Point2f>& pts2d){
        
        if(p2d.rows !=2){cout << "ERROR in convertMatToVecPoint: input matrix dim is != 2" << endl;}
        Point2f tp;
        for(int i=0;i<p2d.cols;i++){
            tp.x = p2d.at<double>(0,i);
            tp.y = p2d.at<double>(1,i);
            pts2d.push_back(tp);
        }
    }
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for reading databases
    ////////////////////////////////////////////////////
    void getImagesFromDataset(const string datasetPath, vector<vector<Mat> >& imageSet){
        
    }
    
    bool getImagesFromDataset(const string datasetPath, vector<vector<Mat> >& imageSet, const int numCams, const int numImgs, const string format, int flag=1){
        //return structure is: image[imageindex][cameraindex];
        vector<Mat> ret_imgs;
        string fullfilepath;
		
        for(int j=0;j<numCams;j++){	
            for(int i=0;i<numImgs;i++){
                stringstream filepath;
                filepath << "cam" << j+1 << "/img" << i+1 << ".";
                fullfilepath = datasetPath + filepath.str() + format;
                cout << "fullfilepath is " << fullfilepath << endl;
                Mat rimg = imread(fullfilepath, flag);
                if(!rimg.data){
                    //				cout << "i adn j" << k << j << endl;
                    return false;
                }
                ret_imgs.push_back(rimg);	
            }		
            imageSet.push_back(ret_imgs);
            ret_imgs.clear();
        }
        return true;
    }
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Saving
    ////////////////////////////////////////////////////
    
    
    void saveFeatures(const std::string& filename, const ImageFeatures& imgst){
        
        int numCams = imgst.multikeypoints.size();
        int numKeypoints = imgst.multikeypoints[0].size();
        
        if(remove(filename.c_str())!=0){cout << "ERROR in saveFeatures(): cannot clear the specific file before writing" << endl;}
        fstream fileptr;
        fileptr.open (filename.c_str(), fstream::out | fstream::app);
        
        //  fileptr << numCams;
        //  fileptr << numKeypoints;
        
        for (int i=0;i<1;i++){
            //  fileptr << "\nCamera: " << i+1 << endl;
            for (int j=0;j<10;j++){
                fileptr << float(imgst.multikeypoints[i][j].pt.x);
                fileptr << float(imgst.multikeypoints[i][j].pt.y);
            }
        }
        fileptr.close();
        /*
         fstream fileptr;
         fileptr.open ("test.txt", fstream::trunc);
         fileptr.open ("test.txt", fstream::in | fstream::out | fstream::app);
         fileptr << imgst.matchIdxs.size() << "\n";
         for(int i =0; i<imgst.matchIdxs.size();i++){
         //  for(int i =0; i<10;i++){
         fileptr << imgst.multikeypoints[0][i].pt.x << " " << imgst.multikeypoints[0][i].pt.y << endl;
         }
         fileptr.close();
         // Make xml: <?xml ..><Hello>World</Hello>
         /*  TiXmlDocument doc;
         TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
         doc.LinkEndChild( decl );
         TiXmlElement * element = new TiXmlElement( "Point Matches" );
         //  doc.LinkEndChild( element );
         for(int i =0; i<imgst.matchIdxs.size();i++){
         doc.LinkEndChild( element );
         element->SetDoubleAttribute("x",imgst.multikeypoints[0][i].pt.x);
         element->SetDoubleAttribute("y",imgst.multikeypoints[0][i].pt.y);
         cout << i << endl;
         }
         //  dump_to_stdout(&doc);
         bool res = doc.SaveFile( filename );
         cout << " I am here at right place " << res << endl;
         //  cout << " I am here at right place " << endl;
         
         delete element;
         element=NULL;
         //  delete dec1;
         //  dec1=NULL;
         /*  TiXmlDocument doc;
         TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
         doc.LinkEndChild( decl );
         
         TiXmlElement * element = new TiXmlElement( "Calibration Data" );
         doc.LinkEndChild( element );
         
         TiXmlText * text = new TiXmlText( "Intrinsic Parameters" );
         element->LinkEndChild( text );
         
         
         doc.SaveFile("temp.xml");
         */
    }
    
    ImageFeatures loadFeatures(const std::string& filename){
        
        int num;
        fstream fileptr;
        fileptr.open (filename.c_str(), fstream::in | fstream::app);
        if(!fileptr){cout << "could not load hte file" << endl;}
        //  fileptr << numCams << "\n" << numKeypoints;
        //  while(!fileptr.eof()){
        fileptr >> num;
        cout << num;// << "\n" << numKeypoints;
        //  }
        fileptr.close();
    }
    /* Save the calibParameters into a file
     *  \retval returns void
     */
    void saveCalibFeatures(const std::string& filename, const CameraParams& cp){
        /*
        TiXmlDocument doc;
        TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
        doc.LinkEndChild( decl );
        
        TiXmlElement * element = new TiXmlElement( "Calibration Data" );
        doc.LinkEndChild( element );
        
        TiXmlText * text = new TiXmlText( "Intrinsic Parameters" );
        element->LinkEndChild( text );
        
        
        doc.SaveFile("temp.xml");
        */
    }
    
    
    ////////////////////////////////////////////////////
    /////// Free functions for Error computation
    ////////////////////////////////////////////////////
    
    
    
    double computeNormalizedCalibrationError(const std::vector<cv::Point2f>& testdata, const CameraParams& cp){
        
        
    }
    
    
    
    //for single image calibration marker type
    double computeReprojectionErr(const std::vector<cv::Point3f>& objdata, const std::vector<cv::Point2f>& testdata, const CameraParams& cp){
        
        double retval;
        
        vector<Point2f> est_testdata;
        //    projectPoints(objdata, cp.multicalibparams[0].R, cp.multicalibparams[0].T,  cp.multicalibparams[0].K,  cp.multicalibparams[0].DCoeff, est_testdata);
        /*
         for (int i=0;i<objdata.size();i++){
         cout << "OPoint" << i+1 << ":" << objdata[i] << endl;
         cout << "IPoint" << i+1 << ":" << testdata[i] << endl;
         cout << "EIPoint" << i+1 << ":" << est_testdata[i] << endl;
         }*/
        
        Point2f diff;
        for(int i=0; i<testdata.size();i++){
            diff += (est_testdata[i]-testdata[i]);
        }
        retval = sqrt(diff.x*diff.x + diff.y*diff.y);
        return retval;
    }
    
    ////////////////////////////////////////////////////
    /////// Free functions for Image Processing
    ////////////////////////////////////////////////////
    
    cv::Mat remap(const cv::Mat& src_img, const CameraParams& cp){
        /*  //opencv example implementation cv::Mat undistorted;
         
         if (mustInitUndistort) { // called once per calibration
         
         cv::initUndistortRectifyMap(
         cameraMatrix,  // computed camera matrix
         distCoeffs,    // computed distortion matrix
         cv::Mat(),     // optional rectification (none)
         cv::Mat(),     // camera matrix to generate undistorted
         cv::Size(640,480),
         //            image.size(),  // size of undistorted
         CV_32FC1,      // type of output map
         map1, map2);   // the x and y mapping functions
         
         mustInitUndistort= false;
         }
         
         // Apply mapping functions
         cv::remap(image, undistorted, map1, map2,
         cv::INTER_LINEAR); // interpolation type
         
         return undistorted;
         */
    }
    
    ////////////////////////////////////////////////////
    /////// Free functions for Multi View Geometry
    ////////////////////////////////////////////////////
    
    
    Mat PfromKRT(const Mat& K, const Mat& R, const Mat& T){
        
        Mat P = Mat(3,4,CV_64F);
        Mat KR = Mat(3,3,CV_64F);
        Mat KT = Mat(3,1,CV_64F);
        
        KR = K*R;
        KT = K*T;
        
        for (int i =0; i< 3; i++){
            P.at<double>(i,3) = KT.at<double>(i,0);
            for (int j =0; j< 3; j++){
                P.at<double>(i,j) = KR.at<double>(i,j);
            }
        }
        return P;
    }
    
    void KRTfromP(const cv::Mat P, cv::Mat& K, cv::Mat& R, cv::Mat& T){
        
    }
    
    
    cv::Mat FfromK1K2RT(const Mat& K1, const Mat& K2, const Mat& R, const Mat& T){
        Mat Ts, retval;
        if(T.cols==1 || T.rows==1)
            convertToFromskewSymMat(T,Ts);
        Mat invk2 = K2.inv();
        retval = invk2.t()*(R*Ts)*K1.inv();
        return retval;
    }
    
    
    void convertToFromskewSymMat(const Mat& src, Mat& dst){
        
        Mat retval;
        //assuming source ot be column vector
        if(src.cols==1){
            //convert vector to skew symmetric matrix
            retval = Mat::zeros(3,3,CV_64F);
            retval.at<double>(0,1) = -1 * src.at<double>(2,0);
            retval.at<double>(1,0) = src.at<double>(2,0);
            retval.at<double>(0,2) = src.at<double>(1,0);
            retval.at<double>(2,0) = -1 * src.at<double>(1,0);
            retval.at<double>(1,2) = -1 * src.at<double>(0,0);
            retval.at<double>(2,1) = src.at<double>(0,0);
        }
        else{
            //convert skew symmetric matrix to vector
            retval = Mat(3,1,CV_64F);
            retval.at<double>(0,0) = src.at<double>(2,1);
            retval.at<double>(1,0) = src.at<double>(0,2);
            retval.at<double>(2,0) = src.at<double>(1,0);
        }
        
    }
    
    
};//namesapce
