/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     ParamTuner.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents:
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#include "facs/utilsFeatCalib.h"
#include "facs/paramtuner.h"
#include <iostream>
#include <Magick++.h>

namespace NorthLight {

using namespace cv;
using namespace Magick;
using namespace std;

ParamTuner::ParamTuner(){

}

ParamTuner::~ParamTuner(){

}

vector<Mat> ParamTuner::addNoise(const vector<Mat>& img, float noiseinDB , NoiseType nt){
	vector<Mat> retimg;
	float noiseinLIN=0.0;
	RNG rng;
				  
	switch(nt){
		case N_TYPE_THERMAL:
			for(int i=0;i<img.size();i++){ 			
				 Mat noise = Mat(img[i].rows, img[i].cols, img[i].type());
				 noiseinLIN= pow(10, noiseinDB/10);
              //  cout << "noise factor" << noiseinLIN << endl;
				//generate random gaussian noise of expected value specified by noiseinLIN	
				 Mat noisyimg = Mat(img[i].rows, img[i].cols, img[i].type());
				 randn(noise,0,noiseinLIN);
            //    cout << noise.row(1) << endl;
				 add(img[i], noise, noisyimg);
				 retimg.push_back(noisyimg);
			 }
		break;
		case N_TYPE_SALTPEPPER:
		
		break;
		default:
		break;
	}
	return retimg;
}

vector<Mat> ParamTuner::addDistortion(const vector<Mat>& img, float Dvalue, DistortionType dt){
	vector<Mat> retimg;
	
//	struct lfLensCalibDistortion
//{
    /** The type of distortion model used */
  //  enum lfDistortionModel Model;
    /** Focal length at which this calibration data was taken (0 - unspecified) */
  //  float Focal;
    /** Distortion coefficients, dependent on model (a,b,c; k1,k2 or omega) */
  //  float Terms [3];
//};


	
	switch(dt){
		case D_TYPE_RADIAL:
			for(int i=0;i<img.size();i++){ 
				Mat newimg;
                //can handle only color images at the moment
                Magick::Image im(img[i].cols, img[i].rows, "BGR", CharPixel, (char *)img[i].data);
                double f= (double)Dvalue/100.0;
                double cpoints[]={0,0,f,1-f};
                im.distort(BarrelInverseDistortion,4,cpoints, false);
                im.write("tempdistort.bmp");
//                    newimg = imread("tempdistort.bmp",0);//read as grayscale
                    newimg = imread("tempdistort.bmp");//read as color
				retimg.push_back(newimg);
			}
		break;
		case D_TYPE_TANGENTIAL:
		break;
	}
	return retimg;
	
}

vector<Mat> ParamTuner::reSize(const vector<Mat>& img, Size size){
	vector<Mat> retimg;
	for(int i=0;i<img.size();i++){ 
		Mat newimg = Mat(size.height, size.width, img[i].type());
		resize(img[i], newimg, size);
		retimg.push_back(newimg);
	}
	return retimg;
}

vector<Mat> ParamTuner::addBlur(const vector<Mat>& img, float factor){
	vector<Mat> retimg;
	for(int i=0;i<img.size();i++){ 
		Mat newimg;
		//factor ==> variance of the gaussian
		GaussianBlur(img[i], newimg, Size(0,0), factor, factor);
		retimg.push_back(newimg);
	}
	return retimg;	
}

};//namespace

