/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     timeutils.cpp
 * Author:   Alexander Eichhorn <echa@ulrik.uio.no>
 * Contents: Time helpers
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */
#include "facs/timeutils.h"
#include <boost/shared_ptr.hpp>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

// deliberatly defined outside namespace
const int64_t BILLION = 1000000000LL;
const int64_t MILLION = 1000000L;

namespace NorthLight {

#ifdef __MACH__

// A singleton class to initialise, access and free nanosecond accurate clocks
// on Darwin. (Note: we also tried the static stack variable singleton variant, 
// which worked nicely under Linux, but crashed on Darwin due to uncontrollable
// static destruction order.)
//
class MachClock {
public:
    MachClock()
    {
        host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    }

    ~MachClock()
    {
        mach_port_deallocate(mach_task_self(), cclock);
    }

    struct timespec now()
    {
        struct timespec ts;
        mach_timespec_t mts;
        clock_get_time(cclock, &mts);
        ts.tv_sec = mts.tv_sec;
        ts.tv_nsec = mts.tv_nsec;
        return ts;
    }

    // singleton access method
    static MachClock* getMachClock()
    {
        if (!instance) instance = new MachClock();
        return instance;
    }

private:
    clock_serv_t         cclock;
    static MachClock*    instance;
};

// singleton instance variable
MachClock* MachClock::instance = NULL;

#endif


struct timespec getTime()
{
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    return MachClock::getMachClock()->now();
#else
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts;
#endif
}

bool isT1largerThanT2PlusOfsInUSec(struct timespec ts1, struct timespec ts2,
                                   uint32_t ts2_offset_in_usec)
{
    struct timespec ts3;
    ts3.tv_sec = ts2.tv_sec + ts2_offset_in_usec / MILLION;
    ts3.tv_nsec = ts2.tv_nsec + (ts2_offset_in_usec % MILLION) * 1000;
    if (ts3.tv_nsec > BILLION)
    {
        ts3.tv_nsec -= BILLION;
        ++ts3.tv_sec;
    }

    return ts3.tv_sec < ts1.tv_sec ||
        (ts3.tv_sec == ts1.tv_sec && ts3.tv_nsec < ts1.tv_nsec);
}

bool isT1largerThanT2PlusOfsInMSec(struct timespec ts1, struct timespec ts2,
                                   uint32_t ts2_offset_in_msec)
{
    struct timespec ts3;
    ts3.tv_sec = ts2.tv_sec + ts2_offset_in_msec / 1000;
    ts3.tv_nsec = ts2.tv_nsec + (ts2_offset_in_msec % 1000) * MILLION;
    if (ts3.tv_nsec > BILLION)
    {
        ts3.tv_nsec -= BILLION;
        ++ts3.tv_sec;
    }

    return ts3.tv_sec < ts1.tv_sec ||
        (ts3.tv_sec == ts1.tv_sec && ts3.tv_nsec < ts1.tv_nsec);
}

bool isT1largerThanT2PlusOfsInSec(struct timespec ts1, struct timespec ts2,
                                  uint32_t ts2_offset_in_sec)
{
    struct timespec ts3;
    ts3.tv_sec = ts2.tv_sec + ts2_offset_in_sec;
    ts3.tv_nsec = ts2.tv_nsec;
    if (ts3.tv_nsec > BILLION)
    {
        ts3.tv_nsec -= BILLION;
        ++ts3.tv_sec;
    }

    return ts3.tv_sec < ts1.tv_sec ||
        (ts3.tv_sec == ts1.tv_sec && ts3.tv_nsec < ts1.tv_nsec);
}

struct timespec ts_sub(struct timespec ts1, struct timespec ts2)
{
    return ts1 - ts2;
}

struct timespec ts_add(struct timespec ts1, struct timespec ts2)
{
    return ts1 + ts2;
}

struct timespec ts_add(struct timespec ts1, uint64_t nsec)
{
    return ts1 + nsec;
}

struct timespec ts_sub(struct timespec ts1, uint64_t nsec)
{
    return ts1 - nsec;
}

} // namespace
