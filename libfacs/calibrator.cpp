/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     calibrator.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

//#include <cstdarg>
#include <inttypes.h>
#include <iostream>
#include "facs/calibrator.h"

namespace NorthLight {

using namespace std;
using namespace cv;
/** Constructor
*/
Calibrator::Calibrator(IntrinsicMethod im)
    :fMeth(FUN_METHOD_EMPTY),
    pMeth(POSE_METHOD_EMPTY),
    iMeth(im){
}

Calibrator::Calibrator(FunMatMethod fm, PoseMethod pm)
    :fMeth(fm),
    pMeth(pm),
    iMeth(INT_METHOD_EMPTY){
}



Calibrator::Calibrator(FunMatMethod fm, IntrinsicMethod im, PoseMethod pm)
    :fMeth(fm),
    pMeth(pm),
    iMeth(im){
}



/** Destructor
 */
Calibrator::~Calibrator(){
}


//TODO required to have default values before use may not include when he does not hav an argument to pass or may think unnecessary
/*bool Calibrator::N_calibrate(CalibFeatures& cf,
                             const Size imgSize,
                             const vector<vector<vector<Point2f> > >& imgpoints,
                             const vector<vector<vector<Point3f> > >& objpoints,
                             const vector<Mat>& Ks,
                             const vector<Mat>& Ds){
*/
bool Calibrator::N_calibrate(CalibFeatures& cf,
                     const cv::Size imgSize,
                     MultiCamPoints* multicampts,
                     const std::vector<cv::Mat>& Ks,
                     const std::vector<cv::Mat>& Ds){

    //check for consistency before usign the data structure of points
    bool validDS = multicampts->checkConsistency();
    if(!validDS){
  //      cout << "ERROR in N_calibrate(): In consistency in the data structure of 2D/3D points" << endl;
        return false;
    }

    bool known3D=false;
    bool knownIntrinsics=false;
    vector<Point3f> op;
    multicampts->get3Dpoints(op);
    if(op.size())
    {known3D = true;}// case of KNOWN 3D POINTS - Markerbased
    else
    {known3D = false;} //Case of UNKNOWN 3D points, relying on 2D multiple viewpoints only - Markerlessbased

    if(!Ks.size() || !Ds.size())
    {knownIntrinsics=false;}
    else
    {knownIntrinsics=true;}


    imageSize=imgSize;

    //only to prepare data for opencv requirements
    vector<vector<Point3f> > cv_objpts;
    vector<vector<Point2f> > cv_imgpts;
    vector<vector<Point2f> > cv_imgpts2;

    vector<Point3f> objpts;
    vector<Point2f> imgpts;
    vector<Point2f> imgpts2;

    vector<Point2f> tpts;
    vector<Point2f> tpts2;

    uint32_t diff=0;

    //Intrinsic + extrinsic for N cameras
    //KNOWN 3D
    //UNKNOWN 3D
    bool retval = false;
    switch(multicampts->getNumCameras()){
        case 0:
            return(false);
            break;
        case 1: // only intrinsics
            //known 3D (INTRINSICS)
            if(known3D && !knownIntrinsics){
                cout << "SINGLE CALIBRATION: KNOWN 3D + UNKNOWN INTRINSICS" << endl;
                //parsing data to suit opencv func
                multicampts->get2Dpoints(0,imgpts);
                multicampts->get3Dpoints(objpts);
                diff = imgpts.size()/objpts.size();

                for (uint32_t i=0;i<diff;i++){
                    cv_objpts.push_back(objpts);

                    for(uint32_t j=i*objpts.size(); j<objpts.size(); j++){
                            tpts.push_back(imgpts[j]);
                    }
                    cv_imgpts.push_back(tpts);
                }
                return(calibIntrinsics(cf, cv_imgpts, cv_objpts));
            }else{cout << "ERROR in N_calibrate(): Check if objectpoints and intrinsic arguments are passed properly" << endl;}

            break;
        case 2:

            if(known3D){
                //known 3D + known/unknown intrinsics (EXTRINSICS)
                cout << "STEREO CALIBRATION: KNOWN 3D + KNOWN / UNKNOWN INTRINSICS" << endl;
                //parsing data to suit opencv func
                multicampts->get2Dpoints(0, imgpts);
                multicampts->get2Dpoints(1, imgpts2);
                multicampts->get3Dpoints(objpts);

                diff = imgpts.size()/objpts.size();

                for (uint32_t i=0;i<diff;i++){
                    cv_objpts.push_back(objpts);
                    for(uint32_t j=i*objpts.size(); j< objpts.size(); j++){
                            tpts.push_back(imgpts[j]);
                            tpts2.push_back(imgpts2[j]);
                    }
                    cv_imgpts.push_back(tpts);
                    cv_imgpts2.push_back(tpts2);
                }
                return(calibCameraPose3Dto2D(cf, cv_imgpts, cv_imgpts2, cv_objpts, Ks, Ds));

            }else if(!known3D && knownIntrinsics){
                //unknown 3D + known intrinsics (EXTRINSICS)
                cout << "STEREO CALIBRATION: UNKNOWN 3D + KNOWN INTRINSICS" << endl;
                multicampts->get2Dpoints(0, imgpts);
                multicampts->get2Dpoints(1, imgpts2);
                return(calibCameraPose2Dto2D(cf, imgpts, imgpts2, Ks, Ds));

            }else if(!known3D && !knownIntrinsics){
                //unknown 3D + unknown intrinsics(AUTO = I + E)
                cout << "STEREO CALIBRATION: UNKNOWN 3D + UNKNOWN INTRINSICS" << endl;
                multicampts->get2Dpoints(0, imgpts);
                multicampts->get2Dpoints(1, imgpts2);
                return(calibAuto(cf, imgpts, imgpts2));

            }else{cout << "ERROR in N_calibrate(): Check if objectpoints and intrinsic arguments are passed correctly" << endl;}
            break;
        default:
            break;
    }

    return retval;
}


/** Calibrate given single/stereo image keypoints
 */
bool Calibrator::calibIntrinsics(CalibFeatures& cf,
                                 const vector<vector<Point2f> >& imgpts,
                                 const vector<vector<Point3f> >& objpts){


/*
cout << "breakpoint single calib\n" << endl;
cout << objpts.size() << objpts[0].size() << endl;
cout << imgpts.size() << imgpts[0].size() << endl;
*/
    bool retval = false;
    CameraParams cp;
    vector<CameraParams> mcp;
    struct calibrationSpeed cspeed;
    struct timespec ts_in;
    double res=0.0;
    vector<Mat> rvec, tvec;

    switch(iMeth){

        case INT_METHOD_BOUGUET:
            ts_in = getTime();
            res = calibrateCamera(objpts, imgpts, imageSize, cp.K, cp.D, rvec, tvec);
            cspeed.calibrationTime = getTime() - ts_in;

            if(res){
                mcp.push_back(cp);
                fillCalibStruct(cf, mcp, cspeed);
                retval = true;
            }
            break;

        case INT_METHOD_ZHANG:
        case INT_METHOD_TSAI:
        case INT_METHOD_HEIKKILA:
        default:
            cout << "ERROR in calibIntrinsics(): Unspecified/Unsupported method for given configuration to calibrate" << endl;
            break;
    }//switch

   return retval;
}

//extrinsics wiht known 3D
bool Calibrator::calibCameraPose3Dto2D(CalibFeatures& cf,
                           const vector<vector<Point2f> >& imgpts1,
                           const vector<vector<Point2f> >& imgpts2,
                           const vector<vector<Point3f> >& objpts,
                           const std::vector<cv::Mat>& Ks,
                           const std::vector<cv::Mat>& Ds){

    bool retval = false;
    CameraParams cp1, cp2;
    CalibFeatures cf1, cf2;
    vector<CameraParams> mcp;
    struct calibrationSpeed cspeedindv;
    struct calibrationSpeed cspeed;
    struct timespec ts_in;

    int flags=0;
    double res = 0.0;
    TermCriteria term_crit;

    ts_in = getTime();
    if(!knownIntrinsics){
         bool res1 = calibIntrinsics(cf1, imgpts1, objpts);
         if(res1){
             res1 = calibIntrinsics(cf2, imgpts2, objpts);
             if(res1){
                 cp1.K = cf1.multicamparams[0].K;
                 cp1.D = cf1.multicamparams[0].D;
                 cp2.K = cf2.multicamparams[0].K;
                 cp2.D = cf2.multicamparams[0].D;
                 cspeedindv.calibrationTime = cf1.cspeed.calibrationTime + cf2.cspeed.calibrationTime;
            }else{cout << "ERROR in calibIntrinsics(): not able to obtain Intrinsics" << endl;}
        }else{cout << "ERROR in calibIntrinsics(): not able to obtain Intrinsics" << endl;}
    }else{
        cp1.K = Ks[0];
        cp1.D = Ds[0];
        cp2.K = Ks[1];
        cp2.D = Ds[1];
    }

    switch(pMeth){
        case POSE_MEHTHOD_BOUGUET:
            term_crit=TermCriteria(TermCriteria::COUNT+ TermCriteria::EPS, 30, 1e-6);
            flags=CALIB_FIX_INTRINSIC ;
            res = stereoCalibrate(objpts, imgpts1, imgpts2, cp1.K, cp1.D, cp2.K, cp2.D, imageSize, cp2.R, cp2.T, cp2.E, cp2.F, term_crit, flags);
            cspeed.calibrationTime = getTime() - ts_in;
            if(res){
                mcp.push_back(cp1);
                mcp.push_back(cp2);
                fillCalibStruct(cf, mcp, cspeed);
                retval = true;
            }

            break;

        case POSE_METHOD_8POINT:
        case POSE_METHOD_8POINT_BA:
        default:
            cout << "ERROR in calibCameraPose3Dto2D(): Unspecified/Unsupported method for given configuration to calibrate" << endl;
            break;

    }//switch


    return retval;
}


//extrinsics with unknown 3D
bool Calibrator::calibCameraPose2Dto2D(CalibFeatures& cf,
                           const vector<Point2f>& imgpts1,
                           const vector<Point2f>& imgpts2,
                           const vector<Mat>& Ks,
                           const vector<Mat>& Ds){



    bool retval = false;
    struct calibrationSpeed cspeed;
    struct timespec ts_in;
    bool res = false;

    //copy intrinsics into the structure
    CameraParams cp1, cp2;
    vector<CameraParams> mcp;
    cp1.K=Ks[0];
    cp1.D=Ds[0];
    cp2.K=Ks[1];
    cp2.D=Ds[1];

    switch(pMeth){
        case POSE_METHOD_8POINT:

            //recover R & T
            ts_in = getTime();

            res = recoverRT_8pointalgorithm(imgpts1, imgpts2, cp1.K, cp1.D, cp2.K, cp2.D, cp2.R, cp2.T, cp2.E, cp2.F);
            cspeed.calibrationTime = getTime() - ts_in;
            if(res){
                mcp.push_back(cp1);
                mcp.push_back(cp2);
                fillCalibStruct(cf, mcp, cspeed);
                retval = true;
            }

            break;

        case POSE_METHOD_8POINT_BA:
            //recover inital estimates from 8point algorithm
            ts_in = getTime();

            res = recoverRT_8pointalgorithm(imgpts1, imgpts2, cp1.K, cp1.D, cp2.K, cp2.D, cp2.R, cp2.T, cp2.E, cp2.F);

            //full bundle optimization for camera motion and structure



            cspeed.calibrationTime = getTime() - ts_in;
            if(res){
                mcp.push_back(cp1);
                mcp.push_back(cp2);
                fillCalibStruct(cf, mcp, cspeed);
                retval = true;
            }

            break;

        case POSE_MEHTHOD_BOUGUET:
        default:
            cout << "ERROR in calibCameraPose2Dto2D(): Unspecified/Unsupported method for given configuration to calibrate" << endl;
            break;
    }//switch


    return retval;
}

//auto with unknown 3D
bool Calibrator::calibAuto(CalibFeatures& cf,
                           const vector<Point2f>& imgpts1,
                           const vector<Point2f>& imgpts2){
    return false;
}


bool Calibrator::recoverRT_8pointalgorithm(const vector<Point2f>& imgpts1,
                                           const vector<Point2f>& imgpts2,
                                           const Mat& K1, const Mat& D1,
                                           const Mat& K2, const Mat& D2,
                                           Mat& R, Mat& T,
                                           Mat& E, Mat& F){

    Mat u, w, vt, W, Z, r1, r2, t1, t2, scale;

    //estimate f Mat
    F = estimateFunMat(imgpts1, imgpts2);
    E = K2.t() * F * K1;
    SVD::compute(E, w, u, vt, SVD::FULL_UV);

    //modify E
    scale = (w.row(0) + w.row(1))/2;
    w.at<double>(0,0) = scale.at<double>(0,0);
    w.at<double>(1,0) = scale.at<double>(0,0);
    w.at<double>(2,0) = 0.0;
    E = u * Mat::diag(w) * vt;
    SVD::compute(E, w, u, vt, SVD::FULL_UV);

    //hartley matrices
    double aw [3][3] = {{0.0, -1.0, 0.0},
                        {+1.0, 0.0, 0.0},
                        {0.0, 0.0, +1.0}};
    double az [3][3] = {{0.0, +1.0, 0.0},
                        {-1.0, 0.0, 0.0},
                        {0.0, 0.0, 0.0}};
    W = Mat(3,3,CV_64F, aw);
    Z = Mat(3,3,CV_64F, az);

    //Possible Rotations and translations
    r1 = u * W * vt;
    r2 = u * W.t() * vt;
    u.col(0).copyTo(t1);
    u.col(0).copyTo(t2);
    Mat g = (t2.col(0)*-1);
    g.copyTo(t2);

    //generate one 3D point to test is 3D points estiamted lie in the fron or back of the cameras
    Mat P1 = PfromKRT(K1,Mat::eye(3,3,CV_64F),Mat::zeros(3,1,CV_64F));
    Mat P2_1 = PfromKRT(K2,r1,t1);

    vector<Point3f> est_objpts;
    Mat resMat = triangulationLinear(P1, P2_1, imgpts1, imgpts2, est_objpts);

    //use cheirility constraint
    //projection matrix transformation matrix
    Mat Ht = Mat::eye(4,4,CV_64F);
    for (uint32_t i=0;i<3;i++){
        Ht.at<double>(3,i) = -2 * vt.at<double>(i,2);
    }
    Ht.at<double>(3,3) = -1.0;
    Mat Hr = Ht.diag(0);

    double c1,c2;
    c1 = resMat.at<double>(2,0) * resMat.at<double>(3,0);
    Mat p2d = P2_1 * resMat.col(0);
    c2 = p2d.at<double>(2,0) * resMat.at<double>(3,0);

    if(c1>0 && c2>0){ //P2_1
        R=r1;
        T=t1;
    }else if(c1<0 && c2<0){//reflection of P2_1
        R=r1;
        T=t2;
    }else if(c1*c2 < 0){//twist across baseline of P2_1
        Mat hres =Ht*resMat.col(0);
        if((resMat.at<double>(2,0) * hres.at<double>(3,0)) > 0){
            R=r2;
            T=t1;
        }else{
            R=r2;
            T=t2;
        }
    }else{cout << "ERROR in recoverRT_8pointalgorithm(): not able to determine geometrically correct 3dpoint" << endl;}

    return true;
}


Mat Calibrator::estimateFunMat(const vector<Point2f>& imgpts1,
                               const vector<Point2f>& imgpts2){

    switch(fMeth){
        case FUN_METHOD_7POINT:
//            return(findFundamentalMat(im1, im2, CV_FM_7POINT));
            return(findFundamentalMat(Mat(imgpts1), Mat(imgpts2), CV_FM_7POINT));
            break;

        case FUN_METHOD_8POINT:
            return(findFundamentalMat(Mat(imgpts1), Mat(imgpts2), CV_FM_8POINT));
            break;

        case FUN_METHOD_RANSAC:
            return(findFundamentalMat(Mat(imgpts1), Mat(imgpts2), CV_FM_RANSAC));
            break;

        case FUN_METHOD_LMEDS:
            return(findFundamentalMat(Mat(imgpts1), Mat(imgpts2), CV_FM_LMEDS));
            break;

        case FUN_METHOD_5POINT:
        default:
            cout << "ERROR in estimateFunMat(): Unsupported method" << endl;
            break;

    }//switch
    return Mat();
}



//void Calibrator::triangulationLinear(const Mat& P1, const Mat& P2, const vector<Point2f>& imgpts1, const vector<Point2f>& imgpts2, vector<Point3f>& est_objpts){
Mat Calibrator::triangulationLinear(const Mat& P1, const Mat& P2, const vector<Point2f>& imgpts1, const vector<Point2f>& imgpts2, vector<Point3f>& est_objpts){


    uint32_t numpts = imgpts1.size();

    double arr1[2][numpts];
    double arr2[2][numpts];
    for(uint32_t i=0;i<numpts;i++){
        arr1[0][i]=imgpts1[i].x;
        arr1[1][i]=imgpts1[i].y;
        arr2[0][i]=imgpts2[i].x;
        arr2[1][i]=imgpts2[i].y;
    }

    CvMat p2d1 = cvMat(2, numpts, CV_64F, arr1);
    CvMat p2d2 = cvMat(2, numpts, CV_64F, arr2);
    CvMat* p3d = cvCreateMat(3, numpts, CV_64F);
    CvMat* hp3d = cvCreateMat(4, numpts, CV_64F);

    CvMat cv_P1 = P1;
    CvMat cv_P2 = P2;

    cvTriangulatePoints(&cv_P1, &cv_P2 , &p2d1, &p2d2, hp3d);

    cvConvertPointsHomogeneous(hp3d, p3d);
    Point3f pp3;
    for(uint32_t i=0;i<numpts;i++){
        pp3.x = cvmGet(p3d, 0, i);
        pp3.y = cvmGet(p3d, 1, i);
        pp3.z = cvmGet(p3d, 2, i);
        est_objpts.push_back(pp3);
    }
    Mat outMat = Mat(hp3d);
    return outMat;

}

void Calibrator::fillCalibStruct(CalibFeatures& cstruct,
                                const std::vector<CameraParams>& multicamparams,
                                const struct calibrationSpeed& cspeed){
        cstruct.iMeth = iMeth;
        cstruct.pMeth = pMeth;
        cstruct.fMeth = fMeth;
        cstruct.multicamparams = multicamparams;
        cstruct.cspeed = cspeed;
}



}//namesapce Northlight
