/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     performanceevaluator.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#include "facs/performanceevaluator.h"

namespace NorthLight {

using namespace cv;
using namespace std;


PerformanceEvaluator::PerformanceEvaluator(){

}

PerformanceEvaluator::~PerformanceEvaluator(){

}
    
bool PerformanceEvaluator::epipolarConstraintError(const Mat& funmat, const vector<KeyPoint>& keypoints1, const vector<KeyPoint>& keypoints2, double &epiError){
	bool retval=true;
	//assertions
	//keypoint sizes should be equal
	if(keypoints1.size()!=keypoints2.size()){ return false;}
	
	vector<Point2f> pts1, pts2;
	Mat x1, x2;
	KeyPoint::convert(keypoints1, pts1);
	KeyPoint::convert(keypoints2, pts2);
	
	epipolarConstraintError(funmat, pts1, pts2, epiError);
	return retval;

}

bool PerformanceEvaluator::epipolarConstraintError(const Mat& funmat, const vector<Point2f>& pts1, const vector<Point2f>& pts2, double &epiError){
	bool retval=true;
	//assertions
	//keypoint sizes should be equal
	if(pts1.size()!=pts2.size()){ return false;}
	
	Mat x1, x2;
	x1 = convertToHomogeneous(pts1);
	x2 = convertToHomogeneous(pts2);
	
//   cout << "performance evaluator::" << endl;
//cout << x1 << endl;
//cout << x2 << endl;
   
   Mat nr = x2.t() * funmat * x1;
   Mat l1 = funmat.t() * x2;
   Mat l2 = funmat * x1;
   Mat dnr = nr.diag(0);
   
   Mat seg1 = l1.row(0).mul(l1.row(0));
   Mat seg2 = l1.row(2).mul(l1.row(2));
   Mat seg3 = l2.row(0).mul(l2.row(0));
   Mat seg4 = l2.row(2).mul(l2.row(2));

   Mat seg = seg1 + seg2 + seg3 + seg4;
   
   Mat err = dnr.mul(dnr);
   Mat error = err.mul(1/seg.t());
//   cout << dnr << endl;
   Scalar errsum = sum(error);
   epiError = errsum.val[0];///error.rows;	
//   cout << "perf evalautor" << errsum.val[0] << endl;

//   cout << epiError << endl;

	return retval;

}

	

};//namespace

