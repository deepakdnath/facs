/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     featureextractor.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents:
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */
#include <iostream>
#include "facs/featureextractor.h"



namespace NorthLight {

using namespace std;
using namespace cv;

/** Constructor
 */
FeatureExtractor::FeatureExtractor(FeatureDetectorType ft)
:fType(ft), mType(MATCHER_TYPE_EMPTY), patternSize(ft==POINT_FEATURE_TYPE_CORNER?Size(7,7):Size(0,0)){
}

FeatureExtractor::FeatureExtractor(FeatureDetectorType ft, Size pattern)
:fType(ft), mType(MATCHER_TYPE_EMPTY), patternSize(pattern){
}

FeatureExtractor::FeatureExtractor(FeatureDetectorType ft, FeatureMatcherType mt)
:fType(ft), mType(mt), patternSize(ft==POINT_FEATURE_TYPE_CORNER?Size(7,7):Size(0,0)){
}

/*FeatureExtractor::FeatureExtractor(FeatureDetectorType ft, FeatureMatcherType mt, Size pattern)
:fType(ft), mType(mt), patternSize(pattern){
}*/

/** Destructor
 */
FeatureExtractor::~FeatureExtractor(){
}

/*
ImageFeatures FeatureExtractor::featureDetect(vector<IplImage>* vec_src_img){
    ImageFeatures retval;
    if(vec_src_img.empty()){cout << "ERROR in FeatureExtractor::featureDetect-- IplImage image data not available" << endl; return retval;}
    vector<Mat> cv_vec_src_img;
    vector<IplImage>::iterator it;
    for (it=vec_src_img.begin();it<vec_src_img.end();it++){
        cv_vec_src_img.push_back(Mat(*it));
    }
    return(featureDetect(cv_vec_src_img));
}

    */
ImageFeatures FeatureExtractor::N_featureDetect(const vector<Mat>& vec_src_img){
        ImageFeatures retval;
        switch (vec_src_img.size()){
            case 1:
                if(!vec_src_img[0].data){cout << "ERROR in FeatureExtractor::featureDetect-- Mat image data not available" << endl; return retval;}
                retval = featureDetect(vec_src_img[0]);
                break;
            case 2:
                if(!vec_src_img[0].data || !vec_src_img[1].data){cout << "ERROR in FeatureExtractor::featureDetect-- Mat image data not available"  << endl; return retval;}
                retval = featureDetect(vec_src_img[0], vec_src_img[1]);
                break;
            case 0:
            default:
/*                for (int i=0;i<vec_src_img.size();i++){
                    if(!vec_src_img[i].data){cout << "ERROR in FeatureExtractor::featureDetect-- Mat image data not available"  << endl; return retval;}
                    return(featureDetect(vec_src_img));
                }
 */
                              
                break;
        }//switch
    return retval;  
}

    
/** Feature detection given single image of type MAT
* featstruct will be populated if the feature detection is successful
 */
ImageFeatures FeatureExtractor::featureDetect(const Mat& src_img){

    ImageFeatures retval;
    
//  in case POINT_FEATURE_TYPE_CORNER:
    Mat gray_src_img;
    int full_pattern=0;
    vector<Point2f> corners;

//  in case of others
    vector<Point3f> objpoints;
    vector<KeyPoint> keypoints;
    vector<vector<KeyPoint> > multikeypoints;
    struct featuresSpeed fspeed;
    Vec2i mIdx;
    vector<Vec2i> matchIdxs;
    matchIdxs.clear();
//timing computations
    struct timespec ts_in;

    switch(fType){
        case POINT_FEATURE_TYPE_CORNER:
            ts_in = getTime();
            full_pattern=findChessboardCorners(src_img, patternSize, corners,
                         CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
                        + CALIB_CB_FAST_CHECK);

            if(full_pattern){
                fspeed.detectionTime = getTime() - ts_in;
                cvtColor(src_img, gray_src_img, CV_RGB2GRAY );
                cornerSubPix(gray_src_img, corners, Size(11, 11), Size(-1, -1),
                            TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
                fspeed.detectionTime = getTime() - ts_in;
                // initialize the matrix with image and objects values
                for (uint32_t j=0; j<corners.size(); j++){
                    KeyPoint kpts = KeyPoint(corners[j],-1);
                    keypoints.push_back(kpts);
                    Point3f opts = Point3f(j%patternSize.width, j/patternSize.width, 0.0);
                    objpoints.push_back(opts);
                    mIdx[0]=j;
                    mIdx[1]=0;
                    matchIdxs.push_back(mIdx);

                }
                multikeypoints.push_back(keypoints);
                fillFeaturesStruct(retval, multikeypoints, objpoints, true, fspeed, matchIdxs);
//                fillFeaturesStruct(retval, multikeypoints, objpoints, true);
            }
            break;

        case POINT_FEATURE_TYPE_SIFT:
            ts_in = getTime();
            siftDetector(src_img, keypoints);
            fspeed.detectionTime = getTime() - ts_in;
            
            if(bool(keypoints.size())){
                for(uint32_t i=0;i<keypoints.size();i++){
                    mIdx[0]=i;
                    mIdx[1]=0;
                    matchIdxs.push_back(mIdx);
                }
            }
            break;

        case POINT_FEATURE_TYPE_SURF:
            ts_in = getTime();
            surfDetector(src_img, keypoints);
        //    cout << "breakpoint" << endl;
            fspeed.detectionTime = getTime() - ts_in;
            if(bool(keypoints.size())){
                for(uint32_t i=0;i<keypoints.size();i++){
                    mIdx[0]=i;
                    mIdx[1]=0;
                    matchIdxs.push_back(mIdx);
                }
            }
            break;
            
        case POINT_FEATURE_TYPE_STAR:
            ts_in = getTime();
            starDetector(src_img, keypoints);
            //    cout << "breakpoint" << endl;
            fspeed.detectionTime = getTime() - ts_in;
            if(bool(keypoints.size())){
                for(uint32_t i=0;i<keypoints.size();i++){
                    mIdx[0]=i;
                    mIdx[1]=0;
                    matchIdxs.push_back(mIdx);
                }
            }
            break;

        case POINT_FEATURE_TYPE_ORB:
            ts_in = getTime();
            orbDetector(src_img, keypoints);
            //    cout << "breakpoint" << endl;
            fspeed.detectionTime = getTime() - ts_in;
            if(bool(keypoints.size())){
                for(uint32_t i=0;i<keypoints.size();i++){
                    mIdx[0]=i;
                    mIdx[1]=0;
                    matchIdxs.push_back(mIdx);
                }
            }
            break;
            
        case POINT_FEATURE_TYPE_MSER:        // Maximal Stable Extremal Regions
        case POINT_FEATURE_TYPE_FAST:       //
        case POINT_FEATURE_TYPE_GFTT:
        case POINT_FEATURE_TYPE_HARRIS:
        case POINT_FEATURE_TYPE_DENSE:
        case POINT_FEATURE_TYPE_SIMPLEBLOB:
        case POINT_FEATURE_TYPE_GRIDFAST:
        case POINT_FEATURE_TYPE_PYRAMIDSTAR:
        case POINT_FEATURE_TYPE_SIFT_HESS:
        case POINT_FEATURE_TYPE_SIFT_UTKA:
        default:
             cout << "ERROR in FeatureExtractor::featureDetect -- Unsupported - not tested or implemented yet" << endl;
            break;

    }
    
    multikeypoints.push_back(keypoints);
    fillFeaturesStruct(retval, multikeypoints, objpoints, true, fspeed, matchIdxs);
    return retval;
}

/** Feature detection given single image of type IplImage
* featstruct will be populated if the feature detection is successful
 */
ImageFeatures FeatureExtractor::featureDetect(const IplImage* src_img){
    ImageFeatures retval;
    if(src_img==NULL){cout << "ERROR in FeatureExtractor::featureDetect-- IplImage image data not available" << endl; return retval;}
    Mat matimg = Mat(src_img);
    return(featureDetect(matimg));
}

ImageFeatures FeatureExtractor::featureDetect(const vector<Mat>& vec_src_img){
    ImageFeatures retval;
    vector<KeyPoint> keypoints;
    vector<vector<KeyPoint> > newmultikeypoints;

    vector<Vec2i> matchIdxs;//dummy

    //feature detect stereo pairs
    vector<ImageFeatures> vfs;
    for(uint8_t i=0;i<vec_src_img.size()-1;i++){
        vfs.push_back(featureDetect(vec_src_img[i], vec_src_img[i+1]));
    }


    Mat bigMatrix;
    Mat matMatrix = Mat(vfs[0].matchIdxs);
    //find intersection between matches
    for(uint8_t i=1;i<vfs.size();i++){
        Mat compMatrix = Mat(vfs[i].matchIdxs);
//cout << "breakpoint///////////////////////" << endl;

cout << matMatrix.rows << " " << compMatrix.cols << " " << matMatrix.cols << endl;
        //Intersection between matMatrix & compMatrix
        Mat rmat = Mat(1,matMatrix.cols+1, CV_8U);
        Mat nmat;
        for(uint8_t d=0;d<matMatrix.rows;d++){
            for(uint8_t h=0;h<compMatrix.rows;h++){
                if(matMatrix.at<uint8_t>(d,(matMatrix.cols-1))=compMatrix.at<uint8_t>(h,0)){
                    for(uint8_t l=0;l<matMatrix.cols;i++){
                        rmat.at<uint8_t>(0,l) = matMatrix.at<uint8_t>(d,l);
                    }
                    rmat.at<uint8_t>(0,rmat.cols-1) = compMatrix.at<uint8_t>(h,0);
                    nmat.push_back(rmat);
                }
            }
        }
        matMatrix = nmat.clone();
    }

cout << matMatrix << endl;
//retrive keypoints corresponding to computed matches over N images

    for(uint8_t i=0; i<matMatrix.cols-1; i++){ //every frame/image
        for(uint8_t j=0; j<matMatrix.rows; j++){ //every new filtered keypoint of image/frame
            for (uint8_t k=0; k<vfs[i].matchIdxs.size();k++){ //every old keypoint of original image/frame
                if(matMatrix.at<uint8_t>(j,i)==vfs[i].matchIdxs[k][0]){
                    keypoints.push_back(vfs[i].multikeypoints[0][k]);
                }
            }
        }
        newmultikeypoints.push_back(keypoints);
        keypoints.erase(keypoints.begin(), keypoints.end());
    }


    uint8_t i=matMatrix.cols-1; //last column or frame /image
    for(uint8_t j=0; j<matMatrix.rows; j++){ //every new filtered keypoint of image/frame
        for (uint8_t k=0; k<vfs[i-1].matchIdxs.size();k++){ //every old keypoint of original image/frame
            if(matMatrix.at<int>(j,i)==vfs[i-1].matchIdxs[k][0]){
                keypoints.push_back(vfs[i-1].multikeypoints[0][k]);
            }
        }
    }
    newmultikeypoints.push_back(keypoints);
    keypoints.erase(keypoints.begin(), keypoints.end());


    struct featuresSpeed fspeed; //not yet computed
   fillFeaturesStruct(retval, newmultikeypoints, vfs[0].objpoints, true, fspeed, matchIdxs);

    return retval;
}
    
/** Feature detection given stereo image of type MAT
* featstruct will be populated if the feature detection is successful
 */
ImageFeatures FeatureExtractor::featureDetect(const Mat& left_img, const Mat& right_img){

       
    
    ////////////////

    ImageFeatures retval;

//  in case POINT_FEATURE_TYPE_CORNERS
    ImageFeatures fs1, fs2;

//  in case of others
    //detector
   vector<KeyPoint> keypoints_1, keypoints_2;
    vector<vector<KeyPoint> > multikeypoints;

    //descriptor extract
    Mat descriptor_1, descriptor_2;
    //matcher
    vector<DMatch> matches;
    vector<DMatch> good_matches;
  
    Vec2i mIdx;
    vector<Vec2i> matchIdxs;

    struct featuresSpeed fspeed;
    struct timespec ts_in;

   /* 
    SiftFeatureDetector detector;    
    detector.detect( left_img, keypoints_1 );
    detector.detect( right_img, keypoints_2 );
    //-- Step 2: Calculate descriptors (feature vectors)
    SiftDescriptorExtractor extractor;    
    extractor.compute( left_img, keypoints_1, descriptor_1 );
    extractor.compute( right_img, keypoints_2, descriptor_2 );    
    //-- Step 3: Matching descriptor vectors with a brute force matcher
    //    BruteForceMatcher< L2<float> > matcher;
    FlannBasedMatcher matcher;
    matcher.match( descriptor_1, descriptor_2, matches );    
    double max_dist = 0; double min_dist = 100;
    //-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descriptor_1.rows; i++ )
    { double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }
    //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist )
    //-- PS.- radiusMatch can also be used here.
    for( int i = 0; i < descriptor_1.rows; i++ )
    { if( matches[i].distance < 2*min_dist )
    { good_matches.push_back( matches[i]); }
    }
    //-- Draw matches
    Mat img_matches;
    imshow("freshMatches",1);
    //drawMatches( inImage1, keypoints_1, inImage2, keypoints_2, good_matches, img_matches );
    drawMatches( left_img, keypoints_1, right_img, keypoints_2,
                good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
    //-- Show detected matches
    imshow("freshMatches", img_matches );
    waitKey(0);
    
    */
    //-- Draw matches
    Mat img_matches;



    switch(fType){
        case POINT_FEATURE_TYPE_CORNER:
            fs1 = featureDetect(left_img);
//            cout << fs1.multikeypoints[0].size() << endl;
            if(fs1.isValid){
                fs2 = featureDetect(right_img);
                if(fs2.isValid){
                    for (uint32_t i=0;i<fs1.multikeypoints[0].size();i++){
                        keypoints_1.push_back(fs1.multikeypoints[0][i]);
                        keypoints_2.push_back(fs2.multikeypoints[0][i]);
                        mIdx[0]=mIdx[1]=i;
                        matchIdxs.push_back(mIdx);
                    }
                }
            }
            break;

        case POINT_FEATURE_TYPE_SIFT:
            fs1 = featureDetect(left_img);
            if(fs1.isValid){
                fs2 = featureDetect(right_img);
                if(fs2.isValid){
                    ts_in = getTime();
                    siftExtractor(left_img, fs1.multikeypoints[0], descriptor_1);
                    siftExtractor(right_img, fs2.multikeypoints[0], descriptor_2);
                    fspeed.descriptionTime = getTime() - ts_in;
                    
                  
                }
            }
            break;
        case POINT_FEATURE_TYPE_SURF:
            fs1 = featureDetect(left_img);
            if(fs1.isValid){
                fs2 = featureDetect(right_img);
                if(fs2.isValid){
                    ts_in = getTime();
                    surfExtractor(left_img, fs1.multikeypoints[0], descriptor_1);
                    surfExtractor(right_img, fs2.multikeypoints[0], descriptor_2);
                    fspeed.descriptionTime = getTime() - ts_in;
                }
            }
            break;

        case POINT_FEATURE_TYPE_STAR:
        case POINT_FEATURE_TYPE_ORB:
            fs1 = featureDetect(left_img);
            if(fs1.isValid){
                fs2 = featureDetect(right_img);
                if(fs2.isValid){
                    ts_in = getTime();
                    orbExtractor(left_img, fs1.multikeypoints[0], descriptor_1);
                    orbExtractor(right_img, fs2.multikeypoints[0], descriptor_2);
                    fspeed.descriptionTime = getTime() - ts_in;
                }
            }
            break;

            
            
        case POINT_FEATURE_TYPE_MSER:        // Maximal Stable Extremal Regions
        case POINT_FEATURE_TYPE_FAST:       //
        case POINT_FEATURE_TYPE_GFTT:
        case POINT_FEATURE_TYPE_HARRIS:
        case POINT_FEATURE_TYPE_DENSE:
        case POINT_FEATURE_TYPE_SIMPLEBLOB:
        case POINT_FEATURE_TYPE_GRIDFAST:
        case POINT_FEATURE_TYPE_PYRAMIDSTAR:
        case POINT_FEATURE_TYPE_SIFT_HESS:
        case POINT_FEATURE_TYPE_SIFT_UTKA:
        default:
             cout << "ERROR in FeatureExtractor::featureDetect -- Unsupported - not tested or implemented yet" << endl;
            break;

    }
    
    if(fType!=POINT_FEATURE_TYPE_CORNER && fs1.isValid && fs2.isValid){
        //                 matcher = DescriptorMatcher::create(getMatcherStr());
        ts_in = getTime();
        featureMatch(descriptor_1, descriptor_2, good_matches);
        fspeed.matchingTime = getTime() - ts_in;
        
        for(uint32_t i=0; i<good_matches.size(); i++){
            keypoints_1.push_back(fs1.multikeypoints[0][good_matches[i].queryIdx]);
            keypoints_2.push_back(fs2.multikeypoints[0][good_matches[i].trainIdx]);
            mIdx[0]=good_matches[i].trainIdx;
            mIdx[1]=good_matches[i].queryIdx;
            matchIdxs.push_back(mIdx);
        }   
    }
    
    multikeypoints.push_back(keypoints_1);
    multikeypoints.push_back(keypoints_2);
    fspeed.detectionTime = fs1.fspeed.detectionTime + fs2.fspeed.detectionTime;    
    fillFeaturesStruct(retval, multikeypoints, fs2.objpoints, true, fspeed, matchIdxs);

   
    
    return retval;
}


/** Feature detection given stereo images of type IplImage
* featstruct will be populated if the feature detection is successful
 */
ImageFeatures FeatureExtractor::featureDetect(const IplImage* left_img, const IplImage* right_img){
    ImageFeatures retval;
    if(left_img==NULL || right_img==NULL){cout << "ERROR in FeatureExtractor::featureDetect-- IplImage image data not available"  << endl; return retval;}
    Mat leftmatimg = Mat(left_img);
    Mat rightmatimg = Mat(right_img);
    return(featureDetect(leftmatimg, rightmatimg));
}


/*
ImageFeatures FeatureExtractor::featureDetect(const vector<Mat>& img){
    ImageFeatures retval;
    if(!img1.data || img2.data || img3.data){cout << "ERROR in FeatureExtractor::featureDetect-- Mat image data not available"  << endl; return retval;}

    if(fType==POINT_FEATURE_TYPE_CORNER){cout << "ERROR in FeatureExtractor::featureDetect-- Checkerboard not supported for >2 images" << endl;}

    vector<ImageFeatures> vec_imgfeats;

    for(uint32_t i=0;i<vec_ims.size()-1;i++){
        if(!img[i].data || img[i+1].data){cout << "ERROR in FeatureExtractor::featureDetect-- Mat image data not available"  << endl; return retval;}
        vec_imgfeats.push_back(featureDetect(img[i], img[i+1]);
    }



    return retval;
}
*/
/** Feature detection given triple image of type MAT
* featstruct will be populated if the feature detection is successful
 */
ImageFeatures FeatureExtractor::featureDetect(const Mat& img1, const Mat& img2, const Mat& img3){
    ImageFeatures retval;
    if(!img1.data || img2.data || img3.data){cout << "ERROR in FeatureExtractor::featureDetect-- Mat image data not available"  << endl; return retval;}

    return retval;
}

/** Feature detection given three images of type IplImage
* featstruct will be populated if the feature detection is successful
 */

ImageFeatures FeatureExtractor::featureDetect(const IplImage* img1, const IplImage* img2, const IplImage* img3){
    ImageFeatures retval;
    if(img1==NULL || img2==NULL || img3==NULL){cout << "ERROR in FeatureExtractor::featureDetect-- IplImage image data not available"  << endl; return retval;}
    Mat matimg1 = Mat(img1);
    Mat matimg2 = Mat(img2);
    Mat matimg3 = Mat(img3);
    return(featureDetect(matimg1, matimg2, matimg3));
}



void FeatureExtractor::featureMatch(const Mat& descriptor_1, const Mat& descriptor_2, vector<DMatch>& good_matches){
    
    vector<DMatch> matches;
    double max_dist = 0;
    double min_dist = 100;
    
    switch(mType){
        case MATCHER_TYPE_BRUTEFORCE:
            bruteMatcher(descriptor_1, descriptor_2, matches);            
            break;
        case MATCHER_TYPE_FLANN:
            flannMatcher(descriptor_1, descriptor_2, matches);            
            break;
        case MATCHER_TYPE_BRUTEFORCE_HAMMING:
            bruteHammingMatcher(descriptor_1, descriptor_2, matches);            
            break;
        case MATCHER_TYPE_BRUTEFORCE_L1:
        case MATCHER_TYPE_BRUTEFORCE_HAMMINGLUT:
        default:
            cout << "ERROR in FeatureExtractor::featureMatch -- Unsupported - not tested or implemented yet" << endl;
            break;
    }
    
    
    //-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < descriptor_1.rows; i++ )
    { double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }
    
    //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist )
    //-- PS.- radiusMatch can also be used here.
    //   std::vector< DMatch > good_matches;
    
    for( int i = 0; i < descriptor_1.rows; i++ )
    { if( matches[i].distance < 2*min_dist )
    { good_matches.push_back( matches[i]); }
    }
    
}



//look at videofefatures
string FeatureExtractor::getDetectorStr(){
        switch(fType){
            case POINT_FEATURE_TYPE_SURF:
                return("SURF");
                break;
            case POINT_FEATURE_TYPE_SIFT:
                return("SIFT");
                break;
            default:
                return("");
                break;
        }
}

string FeatureExtractor::getMatcherStr(){
        switch(mType){
            case MATCHER_TYPE_BRUTEFORCE:
                return("BruteForce");
                break;
            case MATCHER_TYPE_BRUTEFORCE_L1:
                return("BruteForce-L1");
                break;
            case MATCHER_TYPE_BRUTEFORCE_HAMMING:
                return("BruteForce-Hamming");
                break;
            case MATCHER_TYPE_BRUTEFORCE_HAMMINGLUT:
                return("BruteForce-HammingLUT");
                break;
            case MATCHER_TYPE_FLANN:
                return("FlannBased");
                break;
            default:
                return("");
                break;
        }
}

    
//detectors    
void FeatureExtractor::siftDetector(const Mat& src_img, vector<KeyPoint>& keypoints){
    SiftFeatureDetector detector;
    detector.detect(src_img, keypoints);
}    

void FeatureExtractor::surfDetector(const Mat& src_img, vector<KeyPoint>& keypoints){
    SurfFeatureDetector detector;
    detector.detect(src_img, keypoints);
}    

void FeatureExtractor::starDetector(const Mat& src_img, vector<KeyPoint>& keypoints){
    StarFeatureDetector detector;
    detector.detect(src_img, keypoints);
//    cout << "keypoint" << keypoints.size() << endl;
}    

void FeatureExtractor::orbDetector(const Mat& src_img, vector<KeyPoint>& keypoints){
    OrbFeatureDetector detector;
    detector.detect(src_img, keypoints);
//    cout << "keypoint" << keypoints.size() << endl;
}    

//descriptor extractors    
void FeatureExtractor::siftExtractor(const Mat& src_img,  vector<KeyPoint>& keypoints, Mat& descriptor ){
    SiftDescriptorExtractor extractor;
    extractor.compute(src_img, keypoints, descriptor);
}

void FeatureExtractor::surfExtractor(const Mat& src_img,  vector<KeyPoint>& keypoints, Mat& descriptor ){
    SurfDescriptorExtractor extractor;
    extractor.compute(src_img, keypoints, descriptor);
}

void FeatureExtractor::orbExtractor(const Mat& src_img,  vector<KeyPoint>& keypoints, Mat& descriptor ){
    OrbDescriptorExtractor extractor;
    extractor.compute(src_img, keypoints, descriptor);
//    cout << "descriptor" << descriptor.size().width << "," <<  descriptor.size().height << endl;
}

//matchers
void FeatureExtractor::bruteMatcher(const Mat& descriptor_1, const Mat& descriptor_2, vector<DMatch>& matches){

//    Ptr<DescriptorMatcher> matcher;
//    matcher->create(getMatcherStr());
    
    BruteForceMatcher< L2<float> > matcher;
    matcher.match(descriptor_1, descriptor_2, matches);
}

void FeatureExtractor::bruteHammingMatcher(const Mat& descriptor_1, const Mat& descriptor_2, vector<DMatch>& matches){
    BruteForceMatcher<Hamming> matcher;
    matcher.match(descriptor_1, descriptor_2, matches);
}
    
void FeatureExtractor::flannMatcher(const Mat& descriptor_1, const Mat& descriptor_2, vector<DMatch>& matches){
//    Ptr<DescriptorMatcher> matcher;
//    matcher->create(getMatcherStr());
    
    FlannBasedMatcher matcher;
    matcher.match(descriptor_1, descriptor_2, matches);
}


/* write the detected feature structure
*  \retval returns nothing
*/
void FeatureExtractor::fillFeaturesStruct(ImageFeatures& fstruct, const vector<vector<KeyPoint> >& multikeypoints,
                const vector<cv::Point3f>& objpoints, const bool validity, const struct featuresSpeed& fspeed, const vector<cv::Vec2i> matchIdxs){
        fstruct.fType = fType;
        fstruct.mType = mType;
        fstruct.multikeypoints = multikeypoints;
        fstruct.objpoints = objpoints;
        fstruct.isValid = validity;
        fstruct.fspeed =  fspeed;
        fstruct.matchIdxs = matchIdxs;
}


}; //namespace


///TODO
//OUTLIER detection usign flann example
//addition fo other features
//storing values
//next calibrate class
//single stereo calibrator storing of values
//lens fun distortion correction
//matchIdxs obsolete


