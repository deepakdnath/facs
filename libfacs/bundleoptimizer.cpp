/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     bundleoptimizer.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: 
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */

#include <facs/utilsFeatCalib.h>
//#include <sba.h>


namespace NorthLight {

class BundleOptimizer{
    public:
        BundleOptimizer(BundleOptimizerType& bt);
        ~BundleOptimizer();

        CameraParams optimize(MultiCamPoints* multicampts, const CameraParams& cp);

    private:
    BundleOptimizerType bType;

};

BundleOptimizer::BundleOptimizer(BundleOptimizerType& bt)
:bType(bt){
}

BundleOptimizer::~BundleOptimizer(){
}

CameraParams BundleOptimizer::optimize(MultiCamPoints* multicampts, const CameraParams& cp){



}


};//namespace
